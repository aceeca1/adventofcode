using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class MarbleNode {
    public MarbleNode Prev, Next;
    public int Worth;
}

class P09 {
    static int Players;
    static int LastMarbleWorth;

    public static long MarbleWinningScore() {
        MarbleNode current = new MarbleNode();
        current.Prev = current;
        current.Next = current;
        current.Worth = 0;
        var score = new long[Players + 1];
        for (int i = 1; i <= LastMarbleWorth; ++i) {
            int currentPlayer = (i + Players - 1) % Players + 1;
            if (i % 23 == 0) {
                score[currentPlayer] += i;
                for (int j = 0; j < 7; ++j) current = current.Prev;
                score[currentPlayer] += current.Worth;
                current = current.Next;
                current.Prev = current.Prev.Prev;
                current.Prev.Next = current;
            } else {
                current = current.Next;
                current.Next = new MarbleNode {
                    Prev = current,
                    Next = current.Next,
                    Worth = i
                };
                current = current.Next;
                current.Next.Prev = current;
            }
        }
        return score.Max();
    }

    public static void Main() {
        var file = new StreamReader("../data/input09.txt");
        var line = file.ReadLine().Split();
        Players = int.Parse(line[0]);
        LastMarbleWorth = int.Parse(line[6]);
        Console.WriteLine(MarbleWinningScore());
        LastMarbleWorth *= 100;
        Console.WriteLine(MarbleWinningScore());
    }
}
