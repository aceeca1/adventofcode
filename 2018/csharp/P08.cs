using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P08 {
    static int[] Input;
    static int Position;

    static int GetAnswer1() {
        var children = Input[Position++];
        var metadata = Input[Position++];
        int answer = 0;
        for (int i = 0; i < children; ++i) answer += GetAnswer1();
        for (int i = 0; i < metadata; ++i) answer += Input[Position++];
        return answer;
    }

    static int GetAnswer2() {
        var children = Input[Position++];
        var metadata = Input[Position++];
        var answers = new int[children];
        int answer = 0;
        if (children == 0) {
            for (int i = 0; i < metadata; ++i) answer += Input[Position++];
            return answer;
        }
        for (int i = 0; i < children; ++i) answers[i] = GetAnswer2();
        for (int i = 0; i < metadata; ++i) {
            var data = Input[Position++];
            if (data == 0 || children < data) continue;
            answer += answers[data - 1];
        }
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input08.txt");
        Input = Array.ConvertAll(file.ReadLine().Split(), int.Parse);
        Position = 0;
        Console.WriteLine(GetAnswer1());
        Position = 0;
        Console.WriteLine(GetAnswer2());
    }
}
