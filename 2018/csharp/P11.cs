using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P11 {
    static int Input;
    static int[,] Grid = new int[301, 301];

    static string GetAnswer1() {
        int maxTotal = int.MinValue, argX = -1, argY = -1;
        for (int i = 1; i <= 300 - 2; ++i) {
            for (int j = 1; j <= 300 - 2; ++j) {
                var total = 0;
                for (int k1 = 0; k1 < 3; ++k1) {
                    for (int k2 = 0; k2 < 3; ++k2) {
                        total += Grid[i + k1, j + k2];
                    }
                }
                if (maxTotal < total) {
                    maxTotal = total;
                    argX = i;
                    argY = j;
                }
            }
        }
        return string.Format("{0},{1}", argX, argY);
    }

    static string GetAnswer2() {
        int[,] PrefixSum = new int[301, 301];
        for (int i = 1; i <= 300; ++i) {
            for (int j = 1; j <= 300; ++j) {
                int a1 = Grid[i, j];
                int a2 = PrefixSum[i - 1, j];
                int a3 = PrefixSum[i, j - 1];
                int a4 = PrefixSum[i - 1, j - 1];
                PrefixSum[i, j] = a1 + a2 + a3 - a4;
            }
        }
        int answer = int.MinValue, argX = -1, argY = -1, argZ = -1;
        for (int z = 1; z <= 300; ++z) {
            for (int i = 1; i <= 300 - z + 1; ++i) {
                for (int j = 1; j <= 300 - z + 1; ++j) {
                    int a1 = PrefixSum[i + z - 1, j + z - 1];
                    int a2 = PrefixSum[i - 1, j + z - 1];
                    int a3 = PrefixSum[i + z - 1, j - 1];
                    int a4 = PrefixSum[i - 1, j - 1];
                    int blockSum = a1 - a2 - a3 + a4;
                    if (answer < blockSum) {
                        answer = blockSum;
                        argZ = z;
                        argX = i;
                        argY = j;
                    }
                }
            }
        }
        return string.Format("{0},{1},{2}", argX, argY, argZ);
    }

    public static void Main() {
        var file = new StreamReader("../data/input11.txt");
        Input = int.Parse(file.ReadLine());
        for (int i = 1; i <= 300; ++i) {
            for (int j = 1; j <= 300; ++j) {
                Grid[i, j] = ((i + 10) * j + Input) * (i + 10) / 100 % 10 - 5;
            }
        }
        Console.WriteLine(GetAnswer1());
        Console.WriteLine(GetAnswer2());
    }
}
