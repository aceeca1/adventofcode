using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P12 {
    static char[] Input, Rule = new char[32];

    static int GetAnswer(int n) {
        var plants = Input;
        var shift = 0;
        for (int i = 0; i < n; ++i) {
            var newPlants = new char[plants.Length + 4];
            for (int j = 0; j < newPlants.Length; ++j) {
                int id = 0;
                for (int k = 0; k < 5; ++k) {
                    int index = j + k - 4;
                    if (index < 0 || plants.Length <= index) continue;
                    id += (plants[index] == '#' ? 1 : 0) << k;
                }
                newPlants[j] = Rule[id];
            }
            plants = newPlants;
            shift += 2;
        }
        int answer = 0;
        for (int i = 0; i < plants.Length; ++i) {
            if (plants[i] == '#') answer += i - shift;
        }
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input12.txt");
        Input = file.ReadLine().Substring(15).ToCharArray();
        file.ReadLine();
        for (int i = 0; i < 32; ++i) {
            var line = file.ReadLine();
            int id = 0;
            for (int j = 0; j < 5; ++j) id += (line[j] == '#' ? 1 : 0) << j;
            Rule[id] = line[9];
        }
        Console.WriteLine(GetAnswer(20));
        long answer1000 = GetAnswer(1000);
        long answer1001 = GetAnswer(1001);
        long d = answer1001 - answer1000;
        Console.WriteLine(answer1000 + d * (50000000000 - 1000));
    }
}
