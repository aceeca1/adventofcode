using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P05 {
    static int React(string s, char c) {
        var stack = new Stack<char>();
        foreach (var i in s) {
            if (i == c || i == (c ^ 32)) continue;
            if (0 < stack.Count && (stack.Peek() ^ i) == 32) {
                stack.Pop();
            } else {
                stack.Push(i);
            }
        }
        return stack.Count;
    }

    public static void Main() {
        var file = new StreamReader("../data/input05.txt");
        var input = file.ReadLine();
        Console.WriteLine(React(input, '\0'));
        var answer = int.MaxValue;
        for (char i = 'a'; i <= 'z'; ++i) {
            var react = React(input, i);
            if (react < answer) answer = react;
        }
        Console.WriteLine(answer);
    }
}
