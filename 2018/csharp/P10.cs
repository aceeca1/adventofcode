using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Point {
    public int X, Y, DX, DY;
}

class P10 {
    static List<Point> Points = new List<Point>();

    static int Range(int time) {
        var minX = Points.Min(k => k.X + k.DX * time);
        var maxX = Points.Max(k => k.X + k.DX * time);
        var minY = Points.Min(k => k.Y + k.DY * time);
        var maxY = Points.Max(k => k.Y + k.DY * time);
        return (maxX - minX) + (maxY - minY);
    }

    static void Show(int time) {
        var minX = Points.Min(k => k.X + k.DX * time);
        var maxX = Points.Max(k => k.X + k.DX * time);
        var minY = Points.Min(k => k.Y + k.DY * time);
        var maxY = Points.Max(k => k.Y + k.DY * time);
        var c = new char[maxX - minX + 1, maxY - minY + 1];
        for (int i = 0; i <= maxX - minX; ++i) {
            for (int j = 0; j <= maxY - minY; ++j) {
                c[i, j] = '.';
            }
        }
        foreach (var i in Points) {
            c[i.X + i.DX * time - minX, i.Y + i.DY * time - minY] = '#';
        }
        for (int i = 0; i <= maxY - minY; ++i) {
            for (int j = 0; j <= maxX - minX; ++j) {
                Console.Write(c[j, i]);
            }
            Console.WriteLine();
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input10.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            Points.Add(new Point {
                X = int.Parse(line.Substring(10, 6).Trim()),
                Y = int.Parse(line.Substring(18, 6).Trim()),
                DX = int.Parse(line.Substring(36, 2).Trim()),
                DY = int.Parse(line.Substring(40, 2).Trim())
            });
        }
        var range0 = Range(0);
        var minRange = int.MaxValue;
        var arg = -1;
        for (int i = 0; ; ++i) {
            var range = Range(i);
            if (range < minRange) {
                minRange = range;
                arg = i;
            }
            if (range0 < range) break;
        }
        Show(arg);
        Console.WriteLine(arg);
    }
}
