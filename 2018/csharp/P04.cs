using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Record : IComparable<Record> {
    const int kWakeUp = -1, kFallAsleep = -2;

    public string Date;
    public int Time, Operation;

    public static Record Parse(string s) {
        var splitted = s.Split();
        var time = splitted[1].Substring(0, splitted[1].Length - 1).Split(':');
        var hour = int.Parse(time[0]);
        var minute = int.Parse(time[1]);
        var operation = 0;
        switch (splitted[2]) {
            case "Guard":
                operation = int.Parse(splitted[3].Substring(1));
                break;
            case "wakes":
                operation = kWakeUp;
                break;
            case "falls":
                operation = kFallAsleep;
                break;
        }
        return new Record {
            Date = splitted[0].Substring(1),
            Time = hour * 60 + minute,
            Operation = operation
        };
    }

    public int CompareTo(Record that) {
        int date = Date.CompareTo(that.Date);
        if (date != 0) return date;
        return Time.CompareTo(that.Time);
    }
}

class P04 {
    const int kMaxTime = 1440;

    static List<Record> Input = new List<Record>();

    public static void Main() {
        var file = new StreamReader("../data/input04.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            Input.Add(Record.Parse(line));
        }
        Input.Sort();
        var sleepStat = new Dictionary<int, int[]>();
        var guard = 0;
        var sleptTime = 0;
        foreach (var i in Input) {
            switch (i.Operation) {
                case -2:
                    sleptTime = i.Time;
                    break;
                case -1:
                    if (!sleepStat.TryGetValue(guard, out var v)) {
                        sleepStat[guard] = new int[kMaxTime];
                    }
                    for (int j = sleptTime; j < i.Time; ) {
                        ++sleepStat[guard][j];
                        if (++j == kMaxTime) j = 0;
                    }
                    break;
                default:
                    guard = i.Operation;
                    break;
            }
        }
        var maxTotal = 0;
        var maxSleep = 0;
        int arg = -1, arg1 = -1, arg2 = -1;
        foreach (var i in sleepStat) {
            var total = i.Value.Sum();
            if (maxTotal < total) {
                maxTotal = total;
                arg = i.Key;
            }
            for (int j = 0; j < kMaxTime; ++j) {
                if (maxSleep < i.Value[j]) {
                    maxSleep = i.Value[j];
                    arg2 = i.Key * j;
                }
            }
        }
        var maxSleep1 = 0;
        for (int i = 0; i < kMaxTime; ++i) {
            if (maxSleep1 < sleepStat[arg][i]) {
                maxSleep1 = sleepStat[arg][i];
                arg1 = i;
            }
        }
        Console.WriteLine(arg * arg1);
        Console.WriteLine(arg2);
    }
}
