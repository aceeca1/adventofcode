using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P02 {
    static List<string> Input = new List<string>();
    
    static int GetAnswer1() {
        var contain2 = 0;
        var contain3 = 0;
        foreach (var i in Input) {
            var count = new int[256];
            foreach (var j in i) ++count[j];
            if (count.Contains(2)) ++contain2;
            if (count.Contains(3)) ++contain3;
        }
        return contain2 * contain3;
    }
    
    static string GetAnswer2() {
        for (int i = 0; i < Input[0].Length; ++i) {
            var h = new HashSet<string>();
            foreach (var j in Input) {
                var s = j.Remove(i, 1);
                if (h.Contains(s)) return s;
                h.Add(s);
            }
        }
        return null;
    }
    
    public static void Main() {
        var file = new StreamReader("../data/input02.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            Input.Add(line);
        }
        Console.WriteLine(GetAnswer1());
        Console.WriteLine(GetAnswer2());
    }
}
