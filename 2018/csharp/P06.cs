using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Coordinate {
    public int X, Y, Label;
}

class P06 {
    static List<Coordinate> C = new List<Coordinate>();
    static HashSet<(int, int)> Admission = new HashSet<(int, int)>();

    static bool Admit(int x, int y) {
        return C.Sum(k => Math.Abs(k.X - x) + Math.Abs(k.Y - y)) < 10000;
    }

    static void DFS(int x, int y) {
        if (Admission.Contains((x, y)) || !Admit(x, y)) return;
        Admission.Add((x, y));
        DFS(x - 1, y);
        DFS(x + 1, y);
        DFS(x, y - 1);
        DFS(x, y + 1);
    }

    public static void Main() {
        var file = new StreamReader("../data/input06.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            var splitted = line.Split();
            C.Add(new Coordinate {
                X = int.Parse(splitted[0].Substring(0, splitted[0].Length - 1)),
                Y = int.Parse(splitted[1]),
                Label = C.Count + 1
            });
        }
        var minX = C.Min(k => k.X);
        var maxX = C.Max(k => k.X);
        var minY = C.Min(k => k.Y);
        var maxY = C.Max(k => k.Y);
        var map = new int[maxX - minX + 1, maxY - minY + 1];
        var seed = C;
        while (0 < seed.Count) {
            var seedNew = new List<Coordinate>();
            var seedFiltered = new List<Coordinate>();
            foreach (var i in seed) {
                if (map[i.X - minX, i.Y - minY] == i.Label) continue;
                if (map[i.X - minX, i.Y - minY] == 0) {
                    map[i.X - minX, i.Y - minY] = i.Label;
                    seedFiltered.Add(i);
                } else {
                    map[i.X - minX, i.Y - minY] = -1;
                }
            }
            foreach (var i in seedFiltered) {
                if (map[i.X - minX, i.Y - minY] == -1) continue;
                if (minX < i.X && map[i.X - 1 - minX, i.Y - minY] == 0) {
                    seedNew.Add(new Coordinate {
                        X = i.X - 1, Y = i.Y,
                        Label = i.Label
                    });
                }
                if (i.X < maxX && map[i.X + 1 - minX, i.Y - minY] == 0) {
                    seedNew.Add(new Coordinate {
                        X = i.X + 1, Y = i.Y,
                        Label = i.Label
                    });
                }
                if (minY < i.Y && map[i.X - minX, i.Y - 1 - minY] == 0) {
                    seedNew.Add(new Coordinate {
                        X = i.X, Y = i.Y - 1,
                        Label = i.Label
                    });
                }
                if (i.Y < maxY && map[i.X - minX, i.Y + 1 - minY] == 0) {
                    seedNew.Add(new Coordinate {
                        X = i.X, Y = i.Y + 1,
                        Label = i.Label
                    });
                }
            }
            seed = seedNew;
        }
        var isInf = new bool[C.Count + 1];
        for (int i = 0; i <= maxX - minX; ++i) {
            if (map[i, 0] != -1) isInf[map[i, 0]] = true;
            if (map[i, maxY - minY] != -1) isInf[map[i, maxY - minY]] = true;
        }
        for (int i = 0; i <= maxY - minY; ++i) {
            if (map[0, i] != -1) isInf[map[0, i]] = true;
            if (map[maxX - minX, i] != -1) isInf[map[maxX - minX, i]] = true;
        }
        var count = new int[C.Count + 1];
        for (int i = 0; i <= maxX - minX; ++i) {
            for (int j = 0; j <= maxY - minY; ++j) {
                if (map[i, j] != -1) ++count[map[i, j]];
            }
        }
        var maxCount = 0;
        for (int i = 1; i <= C.Count; ++i) {
            if (isInf[i]) continue;
            if (maxCount < count[i]) maxCount = count[i];
        }
        Console.WriteLine(maxCount);
        for (int i = minX; i <= maxX; ++i) {
            for (int j = minY; j <= maxY; ++j) {
                DFS(i, j);
            }
        }
        Console.WriteLine(Admission.Count);
    }
}
