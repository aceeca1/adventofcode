using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class P07 {
    const int kFriends = 5;

    static List<int>[] Out = new List<int>[26];

    static string GetAnswer1() {
        var inDegree = new int[26];
        var q = new SortedSet<int>();
        for (int i = 0; i < 26; ++i) {
            foreach (var j in Out[i]) ++inDegree[j];
        }
        for (int i = 0; i < 26; ++i) {
            if (inDegree[i] == 0) q.Add(i);
        }
        var answer = new StringBuilder();
        while (0 < q.Count) {
            var qMin = q.Min;
            answer.Append((char)('A' + qMin));
            q.Remove(qMin);
            foreach (var i in Out[qMin]) {
                if (--inDegree[i] == 0) q.Add(i);
            }
        }
        return answer.ToString();
    }

    static int GetAnswer2() {
        var inDegree = new int[26];
        var q = new SortedSet<int>();
        for (int i = 0; i < 26; ++i) {
            foreach (var j in Out[i]) ++inDegree[j];
        }
        for (int i = 0; i < 26; ++i) {
            if (inDegree[i] == 0) q.Add(i);
        }
        var time = new int[kFriends];
        var finish = new int[kFriends];
        while (true) {
            var minTime = time.Min();
            for (int j = 0; j < kFriends; ++j) {
                if (time[j] != minTime) continue;
                if (finish[j] != 0) {
                    foreach (var k in Out[finish[j] - 1]) {
                        if (--inDegree[k] == 0) q.Add(k);
                    }
                }
            }
            for (int j = 0; j < kFriends; ++j) {
                if (time[j] != minTime) continue;
                if (q.Count != 0) {
                    var qMin = q.Min;
                    q.Remove(qMin);
                    time[j] += 61 + qMin;
                    finish[j] = 1 + qMin;
                } else {
                    if (time.Any(k => k != minTime)) {
                        var minTime2 = time.Where(k => k != minTime).Min();
                        time[j] = minTime2;
                        finish[j] = 0;
                    } else {
                        return minTime;
                    }
                }
            }
        }
    }

    public static void Main() {
        for (int i = 0; i < 26; ++i) Out[i] = new List<int>();
        var file = new StreamReader("../data/input07.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            var splitted = line.Split();
            var u1 = splitted[1][0] - 'A';
            var u2 = splitted[7][0] - 'A';
            Out[u1].Add(u2);
        }
        Console.WriteLine(GetAnswer1());
        Console.WriteLine(GetAnswer2());
    }
}
