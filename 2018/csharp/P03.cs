using System;
using System.Collections.Generic;
using System.IO;

class Box {
    public int StartX, StartY, LengthX, LengthY;

    public static Box Parse(string s) {
        var splited = s.Split();
        splited[2] = splited[2].Split(':')[0];
        var splitedStart = splited[2].Split(',');
        var splitedLength = splited[3].Split('x');
        return new Box {
            StartX = int.Parse(splitedStart[0]),
            StartY = int.Parse(splitedStart[1]),
            LengthX = int.Parse(splitedLength[0]),
            LengthY = int.Parse(splitedLength[1])
        };
    }
}

class P03 {
    static int[,] Field = new int[1000, 1000];
    static List<Box> Input = new List<Box>();

    static int GetAnswer1() {
        int answer = 0;
        for (int iX = 0; iX < 1000; ++iX) {
            for (int iY = 0; iY < 1000; ++iY) {
                if (Field[iX, iY] >= 2) ++answer;
            }
        }
        return answer;
    }

    static bool IsClear(int no) {
        for (int iX = 0; iX < Input[no].LengthX; ++iX) {
            for (int iY = 0; iY < Input[no].LengthY; ++iY) {
                if (1 < Field[Input[no].StartX + iX, Input[no].StartY + iY]) {
                    return false;
                }
            }
        }
        return true;
    }

    static int GetAnswer2() {
        for (int i = 0; i < Input.Count; ++i) {
            if (IsClear(i)) return i + 1;
        }
        return 0;
    }

    public static void Main() {
        var file = new StreamReader("../data/input03.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            Input.Add(Box.Parse(line));
        }
        foreach (var i in Input) {
            for (int jX = 0; jX < i.LengthX; ++jX) {
                for (int jY = 0; jY < i.LengthY; ++jY) {
                    ++Field[i.StartX + jX, i.StartY + jY];
                }
            }
        }
        Console.WriteLine(GetAnswer1());
        Console.WriteLine(GetAnswer2());
    }
}
