using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P01 {
    static List<int> Input = new List<int>();

    static int GetAnswer1() {
        return Input.Sum();
    }

    static int GetAnswer2() {
        var answer = 0;
        var seen = new HashSet<int>();
        while (true) {
            foreach (var i in Input) {
                answer += i;
                if (seen.Contains(answer)) return answer;
                seen.Add(answer);
            }
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input01.txt");
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            Input.Add(int.Parse(line));
        }
        Console.WriteLine(GetAnswer1());
        Console.WriteLine(GetAnswer2());
    }
}
