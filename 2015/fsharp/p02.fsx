#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input02.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let consume(s: string) =
    let a = s.Split('x') |> Array.map(int)
    System.Array.Sort(a)
    a.[0] * a.[1] * 3 + a.[0] * a.[2] * 2 + a.[1] * a.[2] * 2,
    a.[0] * 2 + a.[1] * 2 + a.[0] * a.[1] * a.[2]
let a = readAll.Select(consume).ToArray()
a.Sum(fst) |> printfn "%d"
a.Sum(snd) |> printfn "%d"
