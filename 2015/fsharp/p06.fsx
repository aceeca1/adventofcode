#!/usr/bin/fsharpi
let (===) a1 a2 = obj.ReferenceEquals(a1, a2)
let file = new System.IO.StreamReader("../data/input06.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
type Split = XSplit | YSplit | NoSplit
[<Struct>]
type Rect =
    val X1: int; val X2: int; val Y1: int; val Y2: int
    new (x1, x2, y1, y2) = { X1 = x1; X2 = x2; Y1 = y1; Y2 = y2; }
    member u.Split(dir: Split, at: int) =
        match dir with
        | XSplit -> Rect(u.X1, at, u.Y1, u.Y2), Rect(at, u.X2, u.Y1, u.Y2)
        | YSplit -> Rect(u.X1, u.X2, u.Y1, at), Rect(u.X1, u.X2, at, u.Y2)
        | _ -> failwith "bad direction: NoSplit"
    member u.Excludes(v: Rect) =
        (u.X2 <= v.X1 || v.X2 <= u.X1) || (u.Y2 <= v.Y1 || v.Y2 <= u.Y1)
    member u.Contains(v: Rect) =
        (u.X1 <= v.X1 && v.X2 <= u.X2) && (u.Y1 <= v.Y1 && v.Y2 <= u.Y2)
    member u.Area = (u.X2 - u.X1) * (u.Y2 - u.Y1)
[<AbstractClass>][<AllowNullLiteral>]
type Box<'TBox> =
    val mutable Dir: Split
    val mutable At: int
    val mutable C1: 'TBox
    val mutable C2: 'TBox
    new (dir, at, c1, c2) = { Dir = dir; At = at; C1 = c1; C2 = c2; }
    abstract member ApplyTo: 'TBox -> 'TBox
    abstract member Push: unit -> unit
    abstract member GenParent: Split * int * 'TBox * 'TBox -> 'TBox
    abstract member Sum: int -> int
[<AllowNullLiteral>]
type PowerBox =
    inherit Box<PowerBox>
    val mutable Toggled: bool
    new (dir, at, c1, c2, toggled) = {
        inherit Box<PowerBox>(dir, at, c1, c2)
        Toggled = toggled
    }
    [<DefaultValue>] static val mutable private on     : PowerBox
    [<DefaultValue>] static val mutable private off    : PowerBox
    [<DefaultValue>] static val mutable private toggle : PowerBox
    static member Init =
        PowerBox.on      <- PowerBox(NoSplit, 0, null, null, false)
        PowerBox.off     <- PowerBox(NoSplit, 0, null, null, false)
        PowerBox.toggle  <- PowerBox(NoSplit, 0, null, null, false)
    static member On =      PowerBox.on
    static member Off =     PowerBox.off
    static member Toggle =  PowerBox.toggle
    member u.ToggleBox =
        if   u === PowerBox.On   then PowerBox.Off
        elif u === PowerBox.Off  then PowerBox.On
        else PowerBox(u.Dir, u.At, u.C1, u.C2, not u.Toggled)
    override u.ApplyTo(v: PowerBox) =
        if u === PowerBox.Toggle then v.ToggleBox else u
    override u.Push() =
        if u.Toggled then
            u.Toggled <- false
            u.C1 <- u.C1.ToggleBox
            u.C2 <- u.C2.ToggleBox
    override u.GenParent(dir, at, c1, c2) = PowerBox(dir, at, c1, c2, false)
    override u.Sum(v: int) = if u === PowerBox.On then v else 0
PowerBox.Init
[<AllowNullLiteral>]
type LightBox =
    inherit Box<LightBox>
    val mutable A: int
    val mutable B: int
    new (dir, at, c1, c2, a, b) = {
        inherit Box<LightBox>(dir, at, c1, c2)
        A = a; B = b
    }
    member u.With(a, b) = LightBox(u.Dir, u.At, u.C1, u.C2, a, b)
    override u.ApplyTo(v: LightBox) = v.With(v.A + u.A, max(u.B)(v.B + u.A))
    override u.Push() =
        u.C1 <- u.ApplyTo u.C1; u.C2 <- u.ApplyTo u.C2; u.A <- 0; u.B <- 0
    override u.GenParent(dir, at, c1, c2) = LightBox(dir, at, c1, c2, 0, 0)
    override u.Sum(v: int) = v * (max u.A u.B)
let rec put<'TBox when 'TBox :> Box<'TBox>>
    (rectOp: Rect, op: 'TBox, rectBox: Rect, box: 'TBox) =
    if rectOp.Excludes(rectBox) || op === box then box
    elif rectOp.Contains(rectBox) then op.ApplyTo(box)
    elif box.Dir <> NoSplit then
        box.Push()
        let r1, r2 = rectBox.Split(box.Dir, box.At)
        box.C1 <- put(rectOp, op, r1, box.C1)
        box.C2 <- put(rectOp, op, r2, box.C2); box
    else
        let mutable ch = op.ApplyTo(box)
        if rectBox.X1 < rectOp.X1 && rectOp.X1 < rectBox.X2 then
            ch <- ch.GenParent(XSplit, rectOp.X1, box, ch)
        if rectBox.Y1 < rectOp.Y1 && rectOp.Y1 < rectBox.Y2 then
            ch <- ch.GenParent(YSplit, rectOp.Y1, box, ch)
        if rectBox.X1 < rectOp.X2 && rectOp.X2 < rectBox.X2 then
            ch <- ch.GenParent(XSplit, rectOp.X2, ch, box)
        if rectBox.Y1 < rectOp.Y2 && rectOp.Y2 < rectBox.Y2 then
            ch <- ch.GenParent(YSplit, rectOp.Y2, ch, box)
        ch
let rec sum<'TBox when 'TBox :> Box<'TBox>>(rect: Rect, box: 'TBox) =
    if box.Dir = NoSplit then box.Sum(rect.Area)
    else
        box.Push()
        let r1, r2 = rect.Split(box.Dir, box.At)
        sum(r1, box.C1) + sum(r2, box.C2)
let root = Rect(0, 1000, 0, 1000)
let mutable rPower = PowerBox.Off
let mutable rLight = LightBox(NoSplit, 0, null, null, 0, 0)
let parse(s: string) =
    let ss = s.Split(' ', ',')
    Rect(int ss.[0], int ss.[3] + 1, int ss.[1], int ss.[4] + 1)
for i in readAll do
    if i.StartsWith("turn on") then
        let rect = parse i.[8 ..]
        rPower <- put(rect, PowerBox.On,        root, rPower)
        rLight <- put(rect, rLight.With(1,  0), root, rLight)
    elif i.StartsWith("turn off") then
        let rect = parse i.[9 ..]
        rPower <- put(rect, PowerBox.Off,       root, rPower)
        rLight <- put(rect, rLight.With(-1, 0), root, rLight)
    else
        let rect = parse i.[7 ..]
        rPower <- put(rect, PowerBox.Toggle,    root, rPower)
        rLight <- put(rect, rLight.With(2,  0), root, rLight)
sum(root, rPower) |> printfn "%d"
sum(root, rLight) |> printfn "%d"
