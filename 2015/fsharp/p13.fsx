#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input13.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let people = System.Collections.Generic.Dictionary<string, int>()
let e = ResizeArray<ResizeArray<int>>()
let people2No s =
    match people.TryGetValue s with
    | true, n -> n
    | _ ->
        people.Add(s, e.Count)
        let eLast = ResizeArray<int>(e.Count + 1)
        for i = 0 to e.Count do eLast.Add(0)
        for i in e do i.Add(0)
        e.Add(eLast)
        e.Count - 1
for s in readAll do
    let ss = s.Split(' ', '.')
    let s1, s2 = people2No ss.[0], people2No ss.[10]
    let distAbs = int ss.[3]
    let dist = if ss.[2] = "gain" then -distAbs else distAbs
    e.[s1].[s2] <- e.[s1].[s2] + dist
    e.[s2].[s1] <- e.[s2].[s1] + dist
let m = Array.init(e.Count)(fun i -> e.[i].Min() >>> 1)
for i = 0 to e.Count - 1 do
    for j = 0 to e.Count - 1 do
        e.[i].[j] <- e.[i].[j] - (m.[i] + m.[j])
let calc() =
    let mutable opt = System.Int32.MaxValue
    let a = [| 0 .. e.Count - 1 |]
    let rec put i cc =
        if i = e.Count then
            let cc1 = cc + e.[a.[0]].[a.[a.Length - 1]]
            if cc1 < opt then opt <- cc1
        elif cc < opt then
            let ai = a.[i]
            for j = e.Count - 1 downto i do
                a.[i] <- a.[j]
                a.[j] <- ai
                put(i + 1)(cc + e.[a.[i - 1]].[a.[i]])
                a.[j] <- a.[i]
    put(1)(0); -opt - (m.Sum() <<< 1) //>>
calc() |> printfn "%d"
let me = people2No "Me"
for i = 0 to e.Count - 2 do
    e.[i].[me] <- -m.[i]
    e.[me].[i] <- -m.[i]
calc() |> printfn "%d"
