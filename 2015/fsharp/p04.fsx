#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input04.txt")
let read = file.ReadLine
let s = read()
let md5er = System.Security.Cryptography.MD5.Create()
let md5(s: string) =
    s |> System.Text.Encoding.ASCII.GetBytes |> md5er.ComputeHash
let rec mining(f, n) =
    if f(md5(s + string n)) then n else mining(f, n + 1)
let f1(i: byte[]) = i.[0] = 0uy && i.[1] = 0uy && (i.[2] &&& 0xf0uy = 0uy)
let f2(i: byte[]) = i.[0] = 0uy && i.[1] = 0uy && i.[2] = 0uy
mining(f1, 1) |> printfn "%d"
mining(f2, 1) |> printfn "%d"
