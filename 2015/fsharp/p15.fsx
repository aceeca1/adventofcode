#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input15.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
type Ingredient(s: string) =
    let ss = s.Split(',', ' ')
    member val Prop = [| int ss.[2]; int ss.[5]; int ss.[8]; int ss.[11]; |]
    member val Calo = int ss.[14]
let ingr = readAll.Select(Ingredient).ToArray()
let calc(f: int -> bool) =
    let mutable opt = 0
    let rec put(n: int, room: int, props: int[], calo: int) =
        if n = ingr.Length then
            let ans = props.Select(max 0) |> Seq.reduce( *)
            if f calo && ans > opt then opt <- ans
        else
            for i = 0 to room do
                let propsN = Array.init(props.Length)(fun k ->
                    props.[k] + i * ingr.[n].Prop.[k])
                put(n + 1, room - i, propsN, calo + i * ingr.[n].Calo)
    put(0, 100, [| 0; 0; 0; 0; |], 0); opt
calc(fun i -> true)     |> printfn "%d"
calc(fun i -> i = 500)  |> printfn "%d"
