#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input20.txt")
let read = file.ReadLine
let getPrime(m) =
    let ret = Array.zeroCreate<int>(m + 1)
    let mutable u = 0
    for i = 2 to m do
        let mutable v = ret.[i]
        if v = 0 then v <- i; ret.[u] <- i; u <- i
        let mutable w = 2
        while w > 0 && i * w <= m do
            ret.[i * w] <- w
            if w >= v then w <- -1 else w <- ret.[w]
    ret.[u] <- m + 1
    ret
let n = read() |> int
let rec findWithin(m) =
    let p = getPrime(m)
    let a = Array.zeroCreate<int>(m + 1)
    let b = Array.zeroCreate<int>(m + 1)
    let mutable ret = -1
    a.[1] <- 1
    for i = 2 to m do
        let v = if p.[i] < i then p.[i] else i
        let i0 = i / v
        if i0 % v = 0 then
            a.[i] <- a.[i0] * v + b.[i0]
            b.[i] <- b.[i0]
        else
            a.[i] <- a.[i0] * (v + 1)
            b.[i] <- a.[i0]
        if ret = -1 && a.[i] * 10 >= n then ret <- i
    if ret = -1 then findWithin(m + m) else ret
findWithin(1024) |> printfn "%d"
let rec findWithin2(m) =
    let a = Array.zeroCreate<int>(m + 1)
    for i = 1 to m do
        let mutable j = i
        let mutable c = 1
        while j <= m && c <= 50 do
            a.[j] <- a.[j] + i
            j <- j + i
            c <- c + 1
    match a |> Seq.tryFindIndex(fun i -> i * 11 >= n) with
    | Some(n) -> n
    | None -> findWithin2(m + m)
findWithin2(1024) |> printfn "%d"
