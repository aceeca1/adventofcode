#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input17.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let n = 150
let a = Array.zeroCreate<int>(n + 1)
a.[0] <- 1
let e = Array.create(n + 1)(System.Int32.MaxValue - 1, 0)
e.[0] <- 0, 1
for s in readAll do
    let si = int s
    for i = n downto si do
        a.[i] <- a.[i] + a.[i - si]
        let v1,  k1 = e.[i]
        let v2p, k2 = e.[i - si]
        let v2      = v2p + 1
        e.[i] <-
            if v1 < v2 then e.[i]
            elif v2 < v1 then v2, k2
            else v1, k1 + k2
a.[n] |> printfn "%d"
snd e.[n] |> printfn "%d"
