#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input18.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let evolve1(a: char[][]) =
    Array.init(a.Length)(fun i ->
        Array.init(a.Length)(fun j ->
            let mutable near = 0
            for ik = i - 1 to i + 1 do
                if 0 <= ik && ik < a.Length then
                    for jk = j - 1 to j + 1 do
                        if 0 <= jk && jk < a.Length && a.[ik].[jk] = '#' then
                            near <- near + 1
            match a.[i].[j] with
            | '#' -> if near = 3 || near = 4 then '#' else '.'
            | '.' -> if near = 3 then '#' else '.'
            | _ -> failwith "bad input"))
let force4(a: char[][]) =
    let k = a.Length - 1
    a.[0].[0] <- '#'; a.[0].[k] <- '#'
    a.[k].[0] <- '#'; a.[k].[k] <- '#'; a
let evolve2 = force4 >> evolve1 >> force4
let rec repeat(f, n, a) =
    if n = 0 then a else repeat(f, n - 1, f(a))
let a = readAll.Select(fun i -> i.ToCharArray()).ToArray()
let count(a: char[][]) = a.Select(fun i -> i.Count(fun c -> c = '#')).Sum()
for ev in [| evolve1; evolve2; |] do
    count(repeat(ev, 100, a)) |> printfn "%d"
