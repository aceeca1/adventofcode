#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input19.txt")
let read = file.ReadLine
let rules = [|
    let mutable s: string = null
    while s <> "" do
        s <- read()
        if s <> "" then
            let ss = s.Split()
            yield ss.[0], ss.[2] |]
let init = read()
let molecules = System.Collections.Generic.HashSet<string>()
let forAllR(s: string, s1: string, s2)(f) =
    let mutable idx = -1
    while idx <> -2 do
        idx <- s.IndexOf(s1, idx + 1)
        if idx = -1 then idx <- -2 else
            let mol1 = s.Substring(0, idx)
            let mol2 = s.Substring(idx + s1.Length)
            System.String.Concat(mol1, s2, mol2) |> f
for s1, s2 in rules do
    forAllR(init, s1, s2)(fun s -> molecules.Add(s) |> ignore)
molecules.Count |> printfn "%d"
type QNode =
    val S: string; val N: int
    new (s, n) = { S = s; N = n }
    interface System.IComparable<QNode> with
        member u.CompareTo v =
            let ret = u.S.Length.CompareTo(v.S.Length)
            if ret <> 0 then ret else u.S.CompareTo(v.S)
let qu = System.Collections.Generic.SortedSet<QNode>()
qu.Add(QNode(init, 0))
molecules.Clear()
molecules.Add(init)
let mutable ans = -1
while ans < 0 && qu.Count > 0 do
    let quH = qu.Min
    qu.Remove(quH) |> ignore
    for s1, s2 in rules do
        forAllR(quH.S, s2, s1)(fun s ->
            if molecules.Contains(s) |> not then
                molecules.Add(s) |> ignore
                qu.Add(QNode(s, quH.N + 1)) |> ignore
                if s = "e" then ans <- quH.N + 1)
printfn "%d" ans
