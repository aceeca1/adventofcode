#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input25.txt")
let read = file.ReadLine
let s = read() |> String.map(fun i ->
    if System.Char.IsDigit(i) then i else ' ')
let opts = System.StringSplitOptions.RemoveEmptyEntries
let rc = s.Split((null: char[]), opts)
let r = rc.[0] |> int
let c = rc.[1] |> int
let p = ((r + c - 1) * (r + c - 2) >>> 1) + c
let rec f(n, ans) =
    if n = 0 then ans else f(n - 1, ans * 252533L % 33554393L)
f(p - 1, 20151125L) |> printfn "%d"
