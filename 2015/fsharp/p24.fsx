#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input24.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let a = readAll.Select(int).ToArray()
let solve(k) =
    let s = a.Sum() / k
    let mutable mLen = System.Int32.MaxValue
    let mutable mQu = System.Double.PositiveInfinity
    let rec put(m, s1, s2, s3, s4, len, qu) =
        let lenP = if m = -1 then len else len + (s - s1 + a.[m] - 1) / a.[m]
        if (lenP, qu) < (mLen, mQu) then
            if m = -1 then
                mLen <- len
                mQu <- qu
            else
                if s1 + a.[m] <= s then
                    put(
                        m - 1, s1 + a.[m], s2, s3, s4,
                        len + 1, qu * float a.[m])
                if s2 + a.[m] <= s then
                    put(m - 1, s1, s2 + a.[m], s3, s4, len, qu)
                if s3 + a.[m] <= s && s2 > 0 then
                    put(m - 1, s1, s2, s3 + a.[m], s4, len, qu)
                if k = 4 && s4 + a.[m] <= s && s2 > 0 && s3 > 0 then
                    put(m - 1, s1, s2, s3, s4 + a.[m], len, qu)
    put(a.Length - 1, 0, 0, 0, 0, 0, 1.0)
    mQu |> printfn "%.0f"
solve(3)
solve(4)
