#!/usr/bin/fsharpi
#r "System.Web.Extensions"
open System.Linq
let file = new System.IO.StreamReader("../data/input12.txt")
let read = file.ReadLine
let all = read()
let parser = System.Web.Script.Serialization.JavaScriptSerializer();;
let a = parser.DeserializeObject all
let rec sum noRed (n:obj) =
    match n with
    | :? string   as n -> 0
    | :? int      as n -> n
    | :? (obj[])  as n -> n.Sum(sum noRed)
    | :? System.Collections.Generic.Dictionary<string, obj> as n ->
        if noRed && n.ContainsValue "red" then 0 else n.Values.Sum(sum noRed)
    | _ -> failwith "bad input"
sum false  a |> printfn "%d"
sum true   a |> printfn "%d"
