#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input14.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let duration = 2503
type Reindeer(s: string) =
    let ss = s.Split()
    member val Speed  = int ss.[3]
    member val Fly    = int ss.[6]
    member val Rest   = int ss.[13]
    member u.Distance(d: int) =
        let d1 = d / (u.Fly + u.Rest) * u.Fly
        let d2 = min(d % (u.Fly + u.Rest))(u.Fly)
        (d1 + d2) * u.Speed
let reindeer = readAll.Select(Reindeer).ToArray()
reindeer.Max(fun i -> i.Distance(duration)) |> printfn "%d"
let a = Array.zeroCreate<int>(reindeer.Length)
for time = 1 to duration do
    let dists = reindeer |> Array.map(fun i -> i.Distance(time))
    let distMax = dists.Max()
    for i = 0 to dists.Length - 1 do
        if dists.[i] = distMax then
            a.[i] <- a.[i] + 1
a.Max() |> printfn "%d"
