#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input08.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let hex2Char(s: string) =
    let hex = System.Globalization.NumberStyles.AllowHexSpecifier
    char(System.Byte.Parse(s, hex))
let parse(s: string) =
    let sb = System.Text.StringBuilder()
    let mutable i = 0
    while i < s.Length do
        match s.[i] with
        | '"' -> i <- i + 1
        | '\\' when s.[i + 1] = '\\' ->
            sb.Append('\\') |> ignore
            i <- i + 2
        | '\\' when s.[i + 1] = '"' ->
            sb.Append('"') |> ignore
            i <- i + 2
        | '\\' ->
            sb.Append(hex2Char s.[i + 2 .. i + 3]) |> ignore
            i <- i + 4
        | c    ->
            sb.Append(c) |> ignore
            i <- i + 1
    sb.ToString()
let unparse(s: string) =
    let sb = System.Text.StringBuilder("\"")
    for i in s do
        match i with
        | '"'   -> sb.Append("\\\"")
        | '\\'  -> sb.Append("\\\\")
        | c     -> sb.Append(c)
        |> ignore
    sb.Append("\"").ToString()
let ans = readAll.Select(fun i ->
    i.Length - parse(i).Length, unparse(i).Length - i.Length).ToArray()
ans.Select(fst).Sum() |> printfn "%d"
ans.Select(snd).Sum() |> printfn "%d"

