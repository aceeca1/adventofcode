#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input22.txt")
let read = file.ReadLine
let humanHp = 50
let humanMp = 500
let bossI =    read().Split().[2] |> int
let damageI =  read().Split().[1] |> int
type QNode =
    val Hp: int; val Mp: int; val Boss: int
    val Sh: int; val Po: int; val Re: int;
    val Hu: bool; val MpUsed: int; val F: int
    new (hp, mp, boss, sh, po, re, hu, mpUsed) = {
        Hp = hp; Mp = mp; Boss = boss;
        Sh = sh; Po = po; Re = re
        Hu = hu; MpUsed = mpUsed
        F = mpUsed + 173 * max(0)(boss - po * 3) / 18
    }
    interface System.IComparable<QNode> with
        member u.CompareTo(v) =
            let ret = compare u.F v.F
            if ret <> 0 then ret else
                compare
                    (u.Hp, u.Mp, u.Boss, u.Sh, u.Po, u.Re, u.Hu, u.MpUsed)
                    (v.Hp, v.Mp, v.Boss, v.Sh, v.Po, v.Re, v.Hu, v.MpUsed)
let solve(k) =
    let qu = System.Collections.Generic.SortedSet<QNode>()
    qu.Add(QNode(humanHp - k, humanMp, bossI, 0, 0, 0, true, 0)) |> ignore
    let mutable ans = -1
    let dec(n) = if n = 0 then 0 else n - 1
    while ans = -1 && qu.Count > 0 do
        let quH = qu.Min
        qu.Remove(quH) |> ignore
        if quH.Boss <= 0 then ans <- quH.MpUsed
        elif quH.Hp > 0 then
            let damage() = max(1)(damageI - (if quH.Sh > 0 then 7 else 0))
            let poison = if quH.Po > 0 then 3 else 0
            let recharge = if quH.Re > 0 then 101 else 0
            if quH.Hu then
                if quH.Mp >= 53 then
                    QNode(
                        quH.Hp, quH.Mp + recharge - 53,
                        quH.Boss - poison - 4,
                        dec(quH.Sh), dec(quH.Po), dec(quH.Re),
                        false, quH.MpUsed + 53) |> qu.Add |> ignore
                if quH.Mp >= 73 then
                    QNode(
                        quH.Hp + 2, quH.Mp + recharge - 73,
                        quH.Boss - poison - 2,
                        dec(quH.Sh), dec(quH.Po), dec(quH.Re),
                        false, quH.MpUsed + 73) |> qu.Add |> ignore
                if quH.Mp >= 113 && quH.Sh <= 1 then
                    QNode(
                        quH.Hp, quH.Mp + recharge - 113,
                        quH.Boss - poison,
                        6, dec(quH.Po), dec(quH.Re),
                        false, quH.MpUsed + 113) |> qu.Add |> ignore
                if quH.Mp >= 173 && quH.Po <= 1 then
                    QNode(
                        quH.Hp, quH.Mp + recharge - 173,
                        quH.Boss - poison,
                        dec(quH.Sh), 6, dec(quH.Re),
                        false, quH.MpUsed + 173) |> qu.Add |> ignore
                if quH.Mp >= 229 && quH.Re <= 1 then
                    QNode(
                        quH.Hp, quH.Mp + recharge - 229,
                        quH.Boss - poison,
                        dec(quH.Sh), dec(quH.Po), 5,
                        false, quH.MpUsed + 229) |> qu.Add |> ignore
            else
                QNode(
                    quH.Hp - k - damage(), quH.Mp + recharge,
                    quH.Boss - poison,
                    dec(quH.Sh), dec(quH.Po), dec(quH.Re),
                    true, quH.MpUsed) |> qu.Add |> ignore
    printfn "%d" ans
solve(0); solve(1)
