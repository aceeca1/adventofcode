#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input23.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let e = readAll.ToArray()
let solve(a, b) =
    let reg = System.Collections.Generic.Dictionary<string, int>()
    reg.Add("a", a)
    reg.Add("b", b)
    let mutable c = 0
    while c < e.Length do
        if e.[c].StartsWith("hlf ") then
            let v = e.[c].[4 ..]
            reg.[v] <- reg.[v] >>> 1
            c <- c + 1
        elif e.[c].StartsWith("tpl ") then
            let v = e.[c].[4 ..]
            reg.[v] <- reg.[v] * 3
            c <- c + 1
        elif e.[c].StartsWith("inc ") then
            let v = e.[c].[4 ..]
            reg.[v] <- reg.[v] + 1
            c <- c + 1
        elif e.[c].StartsWith("jmp ") then
            let v = e.[c].[4 ..] |> int
            c <- c + v
        elif e.[c].StartsWith("jie ") then
            let v = e.[c].[4 ..].Split(',')
            let v1 = v.[1].[1 ..] |> int
            c <- if reg.[v.[0]] &&& 1 = 0 then c + v1 else c + 1
        elif e.[c].StartsWith("jio ") then
            let v = e.[c].[4 ..].Split(',')
            let v1 = v.[1].[1 ..] |> int
            c <- if reg.[v.[0]] = 1 then c + v1 else c + 1
    reg.["b"] |> printfn "%d"
solve(0, 0)
solve(1, 0)
