#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input21.txt")
let read = file.ReadLine
type Player =
    val Hp: int; val Damage: int; val Armor: int
    new (hp, damage, armor) = { Hp = hp; Damage = damage; Armor = armor }
    member u.win(v: Player) =
        let uDamage = max(1)(u.Damage - v.Armor)
        let vDamage = max(1)(v.Damage - u.Armor)
        let rec f(hpU, hpV, turnU) =
            if hpU <= 0 then false
            elif hpV <= 0 then true
            elif turnU then f(hpU, hpV - uDamage, false)
            else f(hpU - vDamage, hpV, true)
        f(u.Hp, v.Hp, true)
let boss =
    let hp =      read().Split().[2] |> int
    let damage =  read().Split().[1] |> int
    let armor =   read().Split().[1] |> int
    Player(hp, damage, armor)
let human = 100
let weapons = [| 8, 4; 10, 5; 25, 6; 40, 7; 74, 8 |]
let armors = [| 13, 1; 31, 2; 53, 3; 75, 4; 102, 5 |]
let rings = [|
    25, 1, 0; 50, 2, 0; 100, 3, 0;
    20, 0, 1; 40, 0, 2; 80, 0, 3 |]
let ans1 =
    let mutable ans = System.Int32.MaxValue
    let put0(cost, damage, armor) =
        if cost < ans && Player(human, damage, armor).win(boss) then
            ans <- cost
    let put1(cost, damage, armor) =
        if cost < ans then
            for iC, iD in weapons do
                put0(cost + iC, damage + iD, armor)
    let put2(cost, damage, armor) =
        if cost < ans then
            put1(cost, damage, armor)
            for iC, iA in armors do
                put1(cost + iC, damage, armor + iA)
    let put3(cost, damage, armor) =
        if cost < ans then
            put2(cost, damage, armor)
            for i = 0 to rings.Length - 1 do
                let iC, iD, iA = rings.[i]
                put2(cost + iC, damage + iD, armor + iA)
                for j = 0 to i - 1 do
                    let jC, jD, jA = rings.[j]
                    put2(cost + iC + jC, damage + iD + jD, armor + iA + jA)
    put3(0, 0, 0)
    printfn "%d" ans
let ans2 =
    let mutable ans = 0
    let put0(cost, damage, armor) =
        if Player(human, damage, armor).win(boss) |> not then
            if cost > ans then ans <- cost
    let put1(cost, damage, armor) =
        if Player(human, damage, armor).win(boss) |> not then
            for iC, iD in weapons do
                put0(cost + iC, damage + iD, armor)
    let put2(cost, damage, armor) =
        if Player(human, damage, armor).win(boss) |> not then
            put1(cost, damage, armor)
            for iC, iA in armors do
                put1(cost + iC, damage, armor + iA)
    let put3(cost, damage, armor) =
        if Player(human, damage, armor).win(boss) |> not then
            put2(cost, damage, armor)
            for i = 0 to rings.Length - 1 do
                let iC, iD, iA = rings.[i]
                put2(cost + iC, damage + iD, armor + iA)
                for j = 0 to i - 1 do
                    let jC, jD, jA = rings.[j]
                    put2(cost + iC + jC, damage + iD + jD, armor + iA + jA)
    put3(0, 0, 0)
    printfn "%d" ans
