#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input01.txt")
let read = file.ReadLine
let mutable c = 0
let a = [|
    for i in read() do
        match i with
        | '(' -> c <- c + 1
        | ')' -> c <- c - 1
        | _ -> failwith "bad input"
        yield c |]
a.[a.Length - 1] |> printfn "%d"
a |> Array.findIndex(fun i -> i < 0) |> (+) 1 |> printfn "%d"
