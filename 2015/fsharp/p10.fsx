#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input10.txt")
let read = file.ReadLine()
let lookNSay(s: string) =
    let sb = System.Text.StringBuilder()
    let mutable ti = 0
    let mutable c = 'x'
    for i in s do
        if i <> c then
            if ti > 0 then sb.Append(ti).Append(c) |> ignore
            ti <- 1
            c <- i
        else
            ti <- ti + 1
    sb.Append(ti).Append(c).ToString()
let rec lookNSayR(s: string, r: int) =
    if r = 0 then s else lookNSayR(lookNSay s, r - 1)
let s = lookNSayR(read, 40)
s.Length |> printfn "%d"
lookNSayR(s, 10).Length |> printfn "%d"
