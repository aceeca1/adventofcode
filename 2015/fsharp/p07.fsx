#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input07.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let dc = System.Collections.Generic.Dictionary<string, Lazy<uint16>>()
let eval(s: string) =
    match System.UInt16.TryParse s with
    | true, n -> n
    | _ -> dc.[s].Force()
let parse(s: string) =
    let ss = s.Split()
    ss.[ss.Length - 1],
    match ss.[1] with
    | _ when ss.[0] = "NOT"  -> lazy(~~~eval(ss.[1]))
    | "->"                   -> lazy(eval(ss.[0]))
    | "AND"                  -> lazy(eval(ss.[0]) &&& eval(ss.[2]))
    | "OR"                   -> lazy(eval(ss.[0]) ||| eval(ss.[2]))
    | "LSHIFT"               -> lazy(eval(ss.[0]) <<< int(eval(ss.[2])))
    | "RSHIFT"               -> lazy(eval(ss.[0]) >>> int(eval(ss.[2])))
    | _ -> failwith "bad input"
let wires = readAll.ToArray()
for i in wires do dc.Add(parse i)
let a = dc.["a"].Force() in a |> printfn "%d"
for i in wires do
    let s, v = parse i
    dc.[s] <- v
dc.["b"] <- lazy(a)
dc.["a"].Force() |> printfn "%d"
