#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input11.txt")
let read = file.ReadLine
let s = read().ToCharArray()
let mutable modified = false
let rec put(i: int, abc: bool, aa: char) =
    if i = s.Length then
        if abc && aa = 'Y' && modified then System.String(s) else null
    else
        let mutable ret = null : string
        while isNull ret && s.[i] <= 'z' do
            if s.[i] <> 'i' && s.[i] <> 'o' && s.[i] <> 'l' then
                let diff1 i = s.[i] = s.[i - 1] + char 1
                let abcN = abc || i >= 2 && diff1 i && diff1(i - 1)
                let aaN =
                    match aa with
                    | 'Y'  -> 'Y'
                    | 'N'  when i >= 1 && s.[i] = s.[i - 1] -> s.[i]
                    | 'N'  -> 'N'
                    | c    when s.[i] = c -> c
                    | c    when i >= 1 && s.[i] = s.[i - 1] -> 'Y'
                    | c    -> c
                ret <- put(i + 1, abcN, aaN)
            else for i = i + 1 to s.Length - 1 do s.[i] <- 'a'
            if isNull ret then
                s.[i] <- s.[i] + char 1
                modified <- true
        if s.[i] > 'z' then s.[i] <- 'a'
        ret
let next() = modified <- false; put(0, false, 'N')
next() |> printfn "%s"
next() |> printfn "%s"
