#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input03.txt")
let read = file.ReadLine
type HSII = System.Collections.Generic.HashSet<int * int>
type Santa(a: HSII) =
    let mutable x = 0
    let mutable y = 0
    do a.Add(0, 0) |> ignore
    member this.Move(c) =
        match c with
        | '>' -> x <- x + 1 | '^' -> y <- y + 1
        | '<' -> x <- x - 1 | 'v' -> y <- y - 1
        | _ -> failwith "bad input"
        a.Add(x, y) |> ignore
let hs1 = HSII()
let hs2 = HSII()
let santa1 = Santa(hs1)
let santa2e = Santa(hs2)
let santa2o = Santa(hs2)
let mutable even = true
for i in read() do
    santa1.Move(i)
    if even then santa2e.Move(i) else santa2o.Move(i)
    even <- not even
hs1.Count |> printfn "%d"
hs2.Count |> printfn "%d"
