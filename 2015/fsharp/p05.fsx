#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input05.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let isVowel c =
    match c with
    | 'a' | 'e' | 'i' | 'o' | 'u' -> true
    | _ -> false
let isBan(c1, c2) =
    match c1 with
    | 'a' | 'c' | 'p' | 'x' when int c1 + 1 = int c2 -> true
    | _ -> false
let nice(s: string) =
    s.Count(fun i -> isVowel i) >= 3 &&
    (seq {0 .. s.Length - 2}).Any(fun i -> s.[i] = s.[i + 1]) &&
    (seq {0 .. s.Length - 2}).All(fun i -> isBan(s.[i], s.[i + 1]) |> not),
    (seq {0 .. s.Length - 3}).Any(fun i -> s.[i] = s.[i + 2]) &&
    let a = System.Collections.Generic.HashSet<int>()
    let rec f(m, x1) =
        m <= s.Length - 1 &&
        let x0 = ((x1 <<< 8) &&& 0xffff) + int s.[m]
        a.Contains(x0) || begin
            a.Add(x1) |> ignore
            f(m + 1, x0)
        end
    f(1, int s.[0])
let a = readAll.Select(nice).ToArray()
a.Count(fun i -> fst i) |> printfn "%d"
a.Count(fun i -> snd i) |> printfn "%d"
