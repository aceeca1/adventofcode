#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input09.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let cities = System.Collections.Generic.Dictionary<string, int>()
let e = ResizeArray<ResizeArray<int>>()
let city2No s =
    match cities.TryGetValue s with
    | true, n -> n
    | _ ->
        cities.Add(s, e.Count)
        let eLast = ResizeArray<int>(e.Count + 1)
        for i = 0 to e.Count do eLast.Add(0)
        for i in e do i.Add(0)
        e.Add(eLast)
        e.Count - 1
for s in readAll do
    let ss = s.Split()
    let s1 = city2No ss.[0]
    let s2 = city2No ss.[2]
    let dist = int ss.[4]
    e.[s1].[s2] <- dist
    e.[s2].[s1] <- dist
let calc sign =
    let a = Array.create(e.Count <<< e.Count)(System.Int32.MaxValue)
    for i = 0 to (1 <<< e.Count) - 1 do
        for j = 0 to e.Count - 1 do
            if ((i >>> j) &&& 1) <> 0 then
                let ji = (j <<< e.Count) + i
                if i = (1 <<< j) then a.[ji] <- 0 else
                    for k = 0 to e.Count - 1 do
                        if k <> j && ((i >>> k) &&& 1) <> 0 then
                            let ki = (k <<< e.Count) + (i ^^^ (1 <<< j))
                            let dis = a.[ki] + (sign e.[j].[k])
                            if dis < a.[ji] then a.[ji] <- dis
    seq {
        for i = 1 to e.Count do yield a.[(i <<< e.Count) - 1]
    } |> Seq.min |> sign |> printfn "%d"
calc(~+); calc(~-)
