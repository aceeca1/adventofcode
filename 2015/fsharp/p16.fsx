#!/usr/bin/fsharpi
open System.Linq
let file = new System.IO.StreamReader("../data/input16.txt")
let read = file.ReadLine
let readAll = seq {
    let mutable s = ""
    while not (isNull s) do
        s <- read()
        if not (isNull s) then yield s
}
let props = [|
    "children:", 3; "cats:", 7; "samoyeds:", 2; "pomeranians:", 3
    "akitas:", 0; "vizslas:", 0; "goldfish:", 5; "trees:", 3; "cars:", 2
    "perfumes:", 1 |] |> dict
let parse(s: string) =
    let ss = s.Split(',', ' ')
    [| ss.[2], int ss.[3]; ss.[5], int ss.[6]; ss.[8], int ss.[9]; |]
let sues = readAll.Select(parse).ToArray()
let cmp1(prop: string, num: int) = num = props.[prop]
let cmp2(prop: string, num: int) =
    match prop with
    | "cats:" | "trees:"            -> num > props.[prop]
    | "pomeranians:" | "goldfish:"  -> num < props.[prop]
    | _                             -> num = props.[prop]
for cmp in [| cmp1; cmp2; |] do
    (sues |> Seq.findIndex(fun i -> i |> Seq.forall cmp)) + 1 |> printfn "%d"
