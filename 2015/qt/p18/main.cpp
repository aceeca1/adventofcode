#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Grid {
    vector<string> v;

    int near9(int x, int y) const {
        int lbx = x ? x - 1 : x;
        int lby = y ? y - 1 : y;
        int ubx = x < (int)v.size() - 1     ? x + 1 : x;
        int uby = y < (int)v[0].size() - 1  ? y + 1 : y;
        int ret = 0;
        for (int i = lbx; i <= ubx; ++i)
            for (int j = lby; j <= uby; ++j)
                ret += v[i][j] == '#';
        return ret;
    }

    void next() {
        vector<string> vN(v);
        for (int i = 0; i < (int)v.size(); ++i)
            for (int j = 0; j < (int)v[0].size(); ++j) {
                int ne = near9(i, j);
                bool b = v[i][j] == '#' ? ne == 3 || ne == 4 : ne == 3;
                vN[i][j] = b ? '#' : '.';
            }
        v = move(vN);
    }

    void fixCorner() {
        v[0][0] = v.back().back() = '#';
        v[0].back() = v.back()[0] = '#';
    }

    int count() const {
        int ret = 0;
        for (int i = 0; i < (int)v.size(); ++i)
            for (int j = 0; j < (int)v[0].size(); ++j)
                ret += v[i][j] == '#';
        return ret;
    }
};

Grid readGrid() {
    Grid ret;
    for (;;) {
        ret.v.emplace_back();
        if (!getline(cin, ret.v.back())) break;
    }
    ret.v.pop_back();
    return ret;
}

int main() {
    Grid a1 = readGrid(), a2(a1);
    for (int i = 0; i < 100; ++i) a1.next();
    for (int i = 0; i < 100; ++i) {
        if (i == 0) a2.fixCorner();
        a2.next();
        a2.fixCorner();
    }
    printf("%d\n", a1.count());
    printf("%d\n", a2.count());
    return 0;
}
