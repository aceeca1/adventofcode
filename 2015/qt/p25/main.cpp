#include <cstdio>
#include <iostream>
#include <cctype>
#include <string>
using namespace std;

struct Solution {
    int r, c;

    Solution() {
        string s;
        getline(cin, s);
        for (int i = 0; i < (int)s.size(); ++i)
            if (!isdigit(s[i])) s[i] = ' ';
        sscanf(s.c_str(), "%d%d", &r, &c);
        int p = ((r + c - 1) * (r + c - 2) >> 1) + c, ans = 20151125;
        for (int i = 2; i <= p; ++i) ans = ans * 252533L % 33554393L;
        printf("%d\n", ans);
    }
};

int main() {
    Solution();
    return 0;
}
