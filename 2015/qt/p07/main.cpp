#include <cstdio>
#include <cstring>
#include <cctype>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

vector<string> split(char* s) {
    vector<string> ans;
    for (auto p = strtok(s, " \n"); p; p = strtok(nullptr, " \n"))
        ans.emplace_back(p);
    return ans;
}

struct Expr {
    enum {Const, Var, Not, And, Or, Lshift, Rshift} op;
    uint16_t a0;
    string a1, a2;
};

struct Exprs {
    unordered_map<string, Expr> e;

    int eval(string s) {
        if (isdigit(s[0])) return stol(s);
        switch (e[s].op) {
            case Expr::Const:   return e[s].a0;
            case Expr::Var:
                e[s].a0 =  eval(e[s].a1); break;
            case Expr::Not:
                e[s].a0 = ~eval(e[s].a1); break;
            case Expr::And:
                e[s].a0 = eval(e[s].a1) &   eval(e[s].a2); break;
            case Expr::Or:
                e[s].a0 = eval(e[s].a1) |   eval(e[s].a2); break;
            case Expr::Lshift:
                e[s].a0 = eval(e[s].a1) <<  eval(e[s].a2); break;
            case Expr::Rshift:
                e[s].a0 = eval(e[s].a1) >>  eval(e[s].a2); break;
        }
        e[s].op = Expr::Const;
        return e[s].a0;
    }
};

void addExpr(Exprs& es, char* s) {
    auto a = split(s);
    Expr e;
    if (a[0] == "NOT") {
        e.op = Expr::Not;     e.a1 = a[1];
    } else if (a[1] == "->") {
        e.op = Expr::Var;     e.a1 = a[0];
    } else if (a[1] == "AND") {
        e.op = Expr::And;     e.a1 = a[0]; e.a2 = a[2];
    } else if (a[1] == "OR") {
        e.op = Expr::Or;      e.a1 = a[0]; e.a2 = a[2];
    } else if (a[1] == "LSHIFT") {
        e.op = Expr::Lshift;  e.a1 = a[0]; e.a2 = a[2];
    } else if (a[1] == "RSHIFT") {
        e.op = Expr::Rshift;  e.a1 = a[0]; e.a2 = a[2];
    }
    es.e.emplace(a.back(), e);
}

int main() {
    Exprs es;
    char s[80];
    while (fgets(s, 80, stdin)) addExpr(es, s);
    Exprs es1(es);
    int ans1 = es.eval("a");
    printf("%d\n", ans1);
    es1.e["b"].op = Expr::Const;
    es1.e["b"].a0 = ans1;
    printf("%d\n", es1.eval("a"));
    return 0;
}


