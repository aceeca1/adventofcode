#include <cstdio>
#include <algorithm>
using namespace std;

int main() {
    int ans = 0, ans1 = 0;
    for (;;) {
        int a[3];
        if (scanf("%dx%dx%d", &a[0], &a[1], &a[2]) < 3) break;
        sort(a, a + 3);
        ans += a[0] * a[1] * 3 + (a[1] * a[2] << 1) + (a[2] * a[0] << 1);
        ans1 += ((a[0] + a[1]) << 1) + a[0] * a[1] * a[2];
    }
    printf("%d\n%d\n", ans, ans1);
}
