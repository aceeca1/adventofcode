#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    int ans = 0, ans1 = 0;
    for (;;) {
        char s[22];
        if (scanf("%s", s) < 1) break;
        int vowel = 0;
        bool twice = false, ban = false;
        for (int i = 0; s[i]; ++i) {
            switch (s[i]) {
                case 'a': case 'e': case 'i': case 'o': case 'u': ++vowel;
            }
            if (s[i] == s[i + 1]) twice = true;
            if (
                (s[i] == 'a' && s[i + 1] == 'b') ||
                (s[i] == 'c' && s[i + 1] == 'd') ||
                (s[i] == 'p' && s[i + 1] == 'q') ||
                (s[i] == 'x' && s[i + 1] == 'y')) ban = true;
        }
        ans += vowel >= 3 && twice && !ban;
        unordered_set<int> u;
        bool xyxy = false, xyx = false;
        for (int i = 0; s[i]; ++i) {
            if (u.find((s[i] << 8) + s[i + 1]) != u.end()) xyxy = true;
            if (i) u.emplace((s[i - 1] << 8) + s[i]);
            if (s[i + 1] && s[i] == s[i + 2]) xyx = true;
        }
        ans1 += xyxy && xyx;
    }
    printf("%d\n%d\n", ans, ans1);
    return 0;
}
