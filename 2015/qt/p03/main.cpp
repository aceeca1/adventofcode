#include <cstdio>
#include <cstdint>
#include <utility>
#include <unordered_set>
using namespace std;

struct Hash {
    size_t operator()(const pair<int, int>& n) const {
        constexpr uint64_t kMul = 0x9ddfea08eb382d69ULL;
        uint64_t a = (n.first ^ n.second) * kMul;
        a ^= a >> 47;
        uint64_t b = (n.second ^ a) * kMul;
        b ^= b >> 47;
        return b * kMul;
    }
};

unordered_set<pair<int, int>, Hash> u;

struct Santa {
    int x = 0, y = 0;

    void go(char c) {
        switch (c) {
            case '>': ++x; break;
            case '^': ++y; break;
            case '<': --x; break;
            case 'v': --y;
        }
        u.emplace(x, y);
    }
};

int main() {
    char s[8800];
    scanf("%s", s);
    u.emplace(0, 0);
    Santa sa;
    for (int i = 0; s[i]; ++i) sa.go(s[i]);
    printf("%d\n", (int)u.size());
    u.clear();
    Santa sa1, sa2;
    for (int i = 0; s[i]; ++i) {
        sa1.go(s[i]);
        swap(sa1, sa2);
    }
    printf("%d\n", (int)u.size());
    return 0;
}
