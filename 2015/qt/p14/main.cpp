#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Reindeer {
    int speed, fly, rest;

    Reindeer(string& s) {
        s.resize(remove_if(s.begin(), s.end(), [](char si){
            return si != ' ' && !isdigit(si);
        }) - s.begin());
        sscanf(s.c_str(), "%d%d%d", &speed, &fly, &rest);
    }

    int distance(int d) {
        int d1 = d / (fly + rest) * fly;
        int d2 = min(d % (fly + rest), fly);
        return (d1 + d2) * speed;
    }
};

int main() {
    int duration = 2503;
    scanf("%d ", &duration);
    vector<Reindeer> re;
    string s;
    while (getline(cin, s)) re.emplace_back(s);
    int disMax = 0;
    for (int i = 0; i < (int)re.size(); ++i) {
        int dis = re[i].distance(duration);
        if (dis > disMax) disMax = dis;
    }
    printf("%d\n", disMax);
    vector<int> a(re.size());
    for (int i = 1; i <= duration; ++i) {
        vector<int> dists(re.size());
        for (int j = 0; j < (int)re.size(); ++j)
            dists[j] = re[j].distance(i);
        int distMax = *max_element(dists.begin(), dists.end());
        for (int j = 0; j < (int)re.size(); ++j)
            a[j] += dists[j] == distMax;
    }
    printf("%d\n", *max_element(a.begin(), a.end()));
    return 0;
}
