#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <unordered_set>
#include <queue>
using namespace std;

typedef pair<string, string> Rule;

struct QNode {
    string s;
    int n;
    bool operator<(const QNode& that) const {
        return s.size() > that.s.size();
    }
};

struct Solution {
    vector<Rule> a;
    string init;

    template <class F>
    void forAllR(
        const string& s, const string& s1,
        const string& s2, F f) {
        int idx = -1;
        for (;;) {
            idx = s.find(s1, idx + 1);
            if (idx == -1) break;
            string mol(s.begin(), s.begin() + idx);
            mol.append(s2);
            mol.append(s.begin() + idx + s1.size(), s.end());
            f(move(mol));
        }
    }

    Solution() {
        for (;;) {
            getline(cin, init);
            if (init.empty()) break;
            int eq = init.find('=');
            string s1 = init.substr(0, eq - 1);
            string s2 = init.substr(eq + 3);
            a.emplace_back(move(s1), move(s2));
        }
        getline(cin, init);
        unordered_set<string> molecules;
        for (Rule& i: a)
            forAllR(init, i.first, i.second, [&](string&& s) {
                molecules.emplace(move(s));
            });
        printf("%d\n", (int)molecules.size());
        molecules.clear();
        priority_queue<QNode> pq;
        pq.emplace(QNode{init, 0});
        molecules.emplace(init);
        int ans = -1;
        while (!pq.empty() && ans == -1) {
            QNode pqH = pq.top();
            pq.pop();
            for (Rule& i: a)
                forAllR(pqH.s, i.second, i.first, [&](string&& s) {
                    if (molecules.count(s)) return;
                    pq.emplace(QNode{s, pqH.n + 1});
                    if (s == "e") ans = pqH.n + 1;
                    molecules.emplace(move(s));
                });
        }
        printf("%d\n", ans);
    }
};

int main() {
    Solution();
    return 0;
}
