#include <cstdio>
#include <cstring>
#include <string>
#include <unordered_map>
using namespace std;

int main() {
    unordered_map<string, int> sue = {
        {"children", 3}, {"cats", 7}, {"samoyeds", 2 }, {"pomeranians", 3},
        {"akitas", 0}, {"vizslas", 0}, {"goldfish", 5}, {"trees", 3},
        {"cars", 2}, {"perfumes", 1}};
    int sue1 = -1, sue2 = -1;
    for (int i = 1; ; ++i) {
        char s[3][81];
        int n[3];
        auto r = scanf("%*s%*s%s%d,%s%d,%s%d",
            s[0], &n[0], s[1], &n[1], s[2], &n[2]);
        if (r < 6) break;
        bool isSue1 = true, isSue2 = true;
        for (int j = 0; j < 3; ++j) {
            int z = strlen(s[j]);
            s[j][z - 1] = 0;
            int a = sue.find(s[j])->second;
            if (a != n[j]) isSue1 = false;
            if (!strcmp(s[j], "cats") || !strcmp(s[j], "trees")) {
                if (!(n[j] > a)) isSue2 = false;
            } else if (!strcmp(s[j], "pomeranians") ||
                !strcmp(s[j], "goldfish")) {
                if (!(n[j] < a)) isSue2 = false;
            } else if (a != n[j]) isSue2 = false;
        }
        if (isSue1) sue1 = i;
        if (isSue2) sue2 = i;
    }
    printf("%d\n%d\n", sue1, sue2);
    return 0;
}
