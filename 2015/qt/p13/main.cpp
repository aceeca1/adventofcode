#include <cstdio>
#include <climits>
#include <vector>
#include <unordered_map>
using namespace std;

typedef unordered_map<string, int> Usi;
typedef vector<int> Vertex;
typedef vector<Vertex> Graph;

int s2no(Usi& no, Graph& g, const char* s) {
    auto p = no.emplace(s, g.size());
    if (p.second) {
        for (int i = 0; i < (int)g.size(); ++i) g[i].emplace_back();
        g.emplace_back(g.size() + 1);
    }
    return p.first->second;
}

struct Perm {
    vector<int> a;
    int ans;

    void reset(const Graph& g) {
        a.resize(g.size());
        for (int i = 0; i < (int)g.size(); ++i) a[i] = i;
        ans = INT_MIN;
    }

    void put(const Graph& g, int m, int su) {
        if (m == (int)g.size()) {
            su += g[a[g.size() - 1]][a[0]];
            if (su > ans) ans = su;
            return;
        }
        for (int i = m; i < (int)g.size(); ++i) {
            swap(a[m], a[i]);
            put(g, m + 1, m ? su + g[a[m]][a[m - 1]] : su);
            swap(a[m], a[i]);
        }
    }
};

int main() {
    Graph g;
    Usi no;
    int d;
    char s1[81], s2[81], verb[81];
    while (scanf("%s would %s%d happiness units by sitting next to %[^.].",
        s1, verb, &d, s2) == 4) {
        if (verb[0] == 'l') d = -d;
        int no1 = s2no(no, g, s1), no2 = s2no(no, g, s2);
        g[no1][no2] = d;
    }
    for (int i = 0; i < (int)g.size(); ++i)
        for (int j = 0; j < i; ++j)
            g[j][i] = g[i][j] += g[j][i];
    Perm p;
    p.reset(g);
    p.put(g, 0, 0);
    printf("%d\n", p.ans);
    for (int i = 0; i < (int)g.size(); ++i) g[i].emplace_back(0);
    g.emplace_back(g.size() + 1);
    p.reset(g);
    p.put(g, 0, 0);
    printf("%d\n", p.ans);
    return 0;
}
