#include <cstdio>
#include <climits>
#include <algorithm>
using namespace std;

struct Player {
    int hp, damage, armor;

    bool win(const Player& that) const {
        int d1 = max(1, damage - that.armor);
        int d2 = max(1, that.damage - armor);
        int hp1 = hp, hp2 = that.hp;
        bool turn1 = true;
        for (;;) {
            if (hp1 <= 0) return false;
            if (hp2 <= 0) return true;
            if (turn1) hp2 -= d1; else hp1 -= d2;
            turn1 = !turn1;
        }
    }

    static Player boss() {
        int hp, damage, armor;
        scanf("Hit Points: %d ", &hp);
        scanf("Damage: %d ", &damage);
        scanf("Armor: %d ", &armor);
        return Player{hp, damage, armor};
    }
};

struct Wields {
    int cost, damage, armor;
};

constexpr Wields weapons[] = {
    {8, 4, 0}, {10, 5, 0}, {25, 6, 0},
    {40, 7, 0}, {74, 8, 0}
};

constexpr Wields armors[] = {
    {13, 0, 1}, {31, 0, 2}, {53, 0, 3},
    {75, 0, 4}, {102, 0, 5}
};

constexpr Wields rings[] = {
    {25, 1, 0}, {50, 2, 0}, {100, 3, 0},
    {20, 0, 1}, {40, 0, 2}, {80, 0, 3}
};
constexpr int ringsZ = sizeof(rings) / sizeof(*rings);

constexpr int human = 100;

struct Solution {
    Player boss = Player::boss();
    int ans1 = INT_MAX, ans2 = 0;

    void put1p0(int cost, int damage, int armor) {
        if (cost >= ans1) return;
        if (!Player{human, damage, armor}.win(boss)) return;
        ans1 = cost;
    }

    void put1p1(int cost, int damage, int armor) {
        if (cost >= ans1) return;
        for (auto i: weapons)
            put1p0(cost + i.cost, damage + i.damage, armor);
    }

    void put1p2(int cost, int damage, int armor) {
        if (cost >= ans1) return;
        put1p1(cost, damage, armor);
        for (auto i: armors)
            put1p1(cost + i.cost, damage, armor + i.armor);
    }

    void put1p3(int cost, int damage, int armor) {
        if (cost >= ans1) return;
        put1p2(cost, damage, armor);
        for (int i = 0; i < ringsZ; ++i) {
            int costI = cost + rings[i].cost;
            int damageI = damage + rings[i].damage;
            int armorI = armor + rings[i].armor;
            put1p2(costI, damageI, armorI);
            for (int j = i + 1; j < ringsZ; ++j) {
                int costJ = costI + rings[j].cost;
                int damageJ = damageI + rings[j].damage;
                int armorJ = armorI + rings[j].armor;
                put1p2(costJ, damageJ, armorJ);
            }
        }
    }

    void put2p0(int cost, int damage, int armor) {
        if (cost <= ans2) return;
        if (Player{human, damage, armor}.win(boss)) return;
        ans2 = cost;
    }

    void put2p1(int cost, int damage, int armor) {
        if (Player{human, damage, armor}.win(boss)) return;
        for (auto i: weapons)
            put2p0(cost + i.cost, damage + i.damage, armor);
    }

    void put2p2(int cost, int damage, int armor) {
        if (Player{human, damage, armor}.win(boss)) return;
        put2p1(cost, damage, armor);
        for (auto i: armors)
            put2p1(cost + i.cost, damage, armor + i.armor);
    }

    void put2p3(int cost, int damage, int armor) {
        if (Player{human, damage, armor}.win(boss)) return;
        put2p2(cost, damage, armor);
        for (int i = 0; i < ringsZ; ++i) {
            int costI = cost + rings[i].cost;
            int damageI = damage + rings[i].damage;
            int armorI = armor + rings[i].armor;
            put2p2(costI, damageI, armorI);
            for (int j = i + 1; j < ringsZ; ++j) {
                int costJ = costI + rings[j].cost;
                int damageJ = damageI + rings[j].damage;
                int armorJ = armorI + rings[j].armor;
                put2p2(costJ, damageJ, armorJ);
            }
        }
    }

    Solution() {
        put1p3(0, 0, 0);
        printf("%d\n", ans1);
        put2p3(0, 0, 0);
        printf("%d\n", ans2);
    }
};

int main() {
    Solution();
    return 0;
}
