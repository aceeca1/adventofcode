#include <cstdio>
#include <string>
using namespace std;

struct Password {
    string s;
    bool modified = false;

    bool next(int m, bool abc, char aa) {
        if (m == (int)s.size()) return abc && aa == 'Y' && modified;
        for (; s[m] <= 'z'; ++s[m]) {
            if (s[m] == 'i' || s[m] == 'o' || s[m] == 'l')
                for (int i = m + 1; i < (int)s.size(); ++i) s[i] = 'a';
            else {
                bool abcN = abc || (m >= 2 &&
                    s[m] == s[m - 1] + 1 &&
                    s[m - 1] == s[m - 2] + 1);
                char aaN = aa;
                if (m && s[m] == s[m - 1] && aa != s[m])
                    aaN = aaN == 'N' ? s[m] : 'Y';
                if (next(m + 1, abcN, aaN)) return true;
            }
            modified = true;
        }
        s[m] = 'a';
        return false;
    }
};

int main() {
    char s[9];
    scanf("%s", s);
    Password pw{s};
    pw.next(0, false, 'N');
    printf("%s\n", pw.s.c_str());
    pw.modified = false;
    pw.next(0, false, 'N');
    printf("%s\n", pw.s.c_str());
    return 0;
}
