#include <cstdio>
#include <climits>
#include <string>
#include <unordered_map>
#include <vector>
#include <utility>
using namespace std;

typedef unordered_map<string, int> Usi;
typedef vector<int> Vertex;
typedef vector<Vertex> Graph;

int a2no(Usi& no, Graph& g, const char* a) {
    auto p = no.emplace(a, g.size());
    if (p.second) {
        for (int i = 0; i < (int)g.size(); ++i) g[i].emplace_back();
        g.emplace_back(g.size() + 1);
    }
    return p.first->second;
}

struct Perm {
    vector<int> a;
    int ans = INT_MAX;

    Perm(const Graph& g): a(g.size()) {
        for (int i = 0; i < (int)g.size(); ++i) a[i] = i;
    }

    void put(const Graph& g, int m, int dis) {
        if (m == (int)g.size()) {
            if (dis < ans) ans = dis;
            return;
        }
        for (int i = m; i < (int)g.size(); ++i) {
            swap(a[m], a[i]);
            put(g, m + 1, m ? dis + g[a[m - 1]][a[m]] : dis);
            swap(a[m], a[i]);
        }
    }
};

int main() {
    Graph g;
    Usi no;
    int d;
    char s1[81], s2[81];
    while (scanf("%s to%s =%d", s1, s2, &d) == 3) {
        int a1 = a2no(no, g, s1), a2 = a2no(no, g, s2);
        g[a1][a2] = g[a2][a1] = d;
    }
    Perm p(g);
    p.put(g, 0, 0);
    printf("%d\n", p.ans);
    for (int i = 0; i < (int)g.size(); ++i)
        for (int j = 0; j < (int)g.size(); ++j)
            g[i][j] = -g[i][j];
    p.ans = INT_MAX;
    p.put(g, 0, 0);
    printf("%d\n", -p.ans);
    return 0;
}
