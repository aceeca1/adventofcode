#include <cstdio>
#include <iostream>
#include <cctype>
#include <vector>
using namespace std;

struct Ingredient {
    int prop[5];

    Ingredient(string& s) {
        for (int i = 0; i < (int)s.size(); ++i)
            if (s[i] != '-' && !isdigit(s[i])) s[i] = ' ';
        sscanf(s.c_str(), "%d%d%d%d%d",
            &prop[0], &prop[1], &prop[2], &prop[3], &prop[4]);
    }
};

struct Calc {
    int opt1 = 0, opt2 = 0;
    vector<Ingredient> ingr;

    void put(int n, int room, vector<int>&& props) {
        if (n == (int)ingr.size()) {
            int ans = 1;
            for (int i = 0; i < 4; ++i) {
                int k = props[i];
                if (k < 0) k = 0;
                ans *= k;
            }
            if (ans > opt1) opt1 = ans;
            if (props[4] == 500 && ans > opt2) opt2 = ans;
            return;
        }
        for (int i = 0; i <= room; ++i) {
            vector<int> props0(5);
            for (int j = 0; j < 5; ++j)
                props0[j] = props[j] + i * ingr[n].prop[j];
            put(n + 1, room - i, move(props0));
        }
    }
};

int main() {
    string s;
    Calc c;
    while (getline(cin, s)) c.ingr.emplace_back(s);
    c.put(0, 100, {0, 0, 0, 0, 0});
    printf("%d\n%d\n", c.opt1, c.opt2);
    return 0;
}
