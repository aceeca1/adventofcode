#include <cstdio>
#include <climits>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Solution {
    vector<string> op;

    void solve(int a, int b) {
        int c = 0;
        while (c < (int)op.size()) {
            if (op[c] == "hlf a") { a >>= 1; ++c; }
            else if (op[c] == "hlf b") { b >>= 1; ++c; }
            else if (op[c] == "tpl a") { a *= 3; ++c; }
            else if (op[c] == "tpl b") { b *= 3; ++c; }
            else if (op[c] == "inc a") { ++a; ++c; }
            else if (op[c] == "inc b") { ++b; ++c; }
            else {
                int delta = INT_MAX;
                char reg = '\0';
                auto opc = op[c].c_str();
                if (sscanf(opc, "jmp %d", &delta) == 1) c += delta;
                if (sscanf(opc, "jie %c,%d", &reg, &delta) == 2) {
                    int v = reg == 'a' ? a : b;
                    if (v & 1) ++c; else c += delta;
                }
                if (sscanf(opc, "jio %c,%d", &reg, &delta) == 2) {
                    int v = reg == 'a' ? a : b;
                    if (v == 1) c += delta; else ++c;
                }
            }
        }
        printf("%d\n", b);
    }

    Solution() {
        string s;
        while (getline(cin, s)) op.emplace_back(move(s));
        solve(0, 0);
        solve(1, 0);
    }
};

int main() {
    Solution();
    return 0;
}
