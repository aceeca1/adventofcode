#include <cstdio>
#include <queue>
#include <algorithm>
using namespace std;

struct QNode {
    int hp, mp, boss, sh, po, re, mpUsed, f;
    bool hu;

    QNode& calcF() {
        f = mpUsed + max(0, boss - po * 3) * 173 / 18;
        return *this;
    }

    bool operator<(const QNode& that) const {
        return f > that.f;
    }
};

struct Solution {
    static constexpr int humanHp = 50, humanMp = 500;
    int bossI, damageI;

    static int dec(int n) {
        return n ? n - 1 : 0;
    }

    void solve(int hard) {
        priority_queue<QNode> qu;
        qu.emplace(QNode{
            humanHp - hard, humanMp, bossI, 0, 0, 0, 0, 0, true
        }.calcF());
        int ans = -1;
        while (!qu.empty()) {
            auto quH = qu.top();
            qu.pop();
            if (quH.boss <= 0) { ans = quH.mpUsed; break; }
            if (quH.hp <= 0) continue;
            int boss = quH.po ? quH.boss - 3 : quH.boss;
            int mp = quH.re ? quH.mp + 101 : quH.mp;
            int sh = dec(quH.sh);
            int po = dec(quH.po);
            int re = dec(quH.re);
            if (quH.hu) {
                if (quH.mp >= 53) qu.emplace(QNode{
                    quH.hp, mp - 53, boss - 4, sh, po, re,
                    quH.mpUsed + 53, 0, false
                }.calcF());
                if (quH.mp >= 73) qu.emplace(QNode{
                    quH.hp + 2, mp - 73, boss - 2, sh, po, re,
                    quH.mpUsed + 73, 0, false
                }.calcF());
                if (quH.mp >= 113 && !sh) qu.emplace(QNode{
                    quH.hp, mp - 113, boss, 6, po, re,
                    quH.mpUsed + 113, 0, false
                }.calcF());
                if (quH.mp >= 173 && !po) qu.emplace(QNode{
                    quH.hp, mp - 173, boss, sh, 6, re,
                    quH.mpUsed + 173, 0, false
                }.calcF());
                if (quH.mp >= 229 && !re) qu.emplace(QNode{
                    quH.hp, mp - 229, boss, sh, po, 5,
                    quH.mpUsed + 229, 0, false
                }.calcF());
            } else {
                int damage = quH.sh ? damageI - 7 : damageI;
                if (damage < 1) damage = 1;
                int hp = quH.hp - hard - damage;
                qu.emplace(QNode{
                    hp, mp, boss, sh, po, re,
                    quH.mpUsed, 0, true
                }.calcF());
            }
        }
        printf("%d\n", ans);
    }

    Solution() {
        scanf("Hit Points: %d ", &bossI);
        scanf("Damage: %d ", &damageI);
        solve(0);
        solve(1);
    }
};

int main() {
    Solution();
    return 0;
}
