#include <cstdio>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QTextStream>
using namespace std;

struct JsonVisitor {
    int ans = 0, ans1 = 0;

    void visit(const QJsonValue& u, bool red) {
        if (u.isDouble()) {
            ans += u.toDouble();
            if (!red) ans1 += u.toInt();
        } else if (u.isArray()) {
            auto ua = u.toArray();
            for (int i = 0; i < ua.size(); ++i) visit(ua[i], red);
        } else if (u.isObject()) {
            auto uo = u.toObject();
            for (auto i: uo) if (i.toString() == "red") red = true;
            for (auto i: uo) visit(i, red);
        }
    }
};

int main() {
    QTextStream qts(stdin, QIODevice::ReadOnly);
    auto content = qts.readAll().toLatin1();
    auto obj = QJsonDocument::fromJson(content).object();
    JsonVisitor v;
    v.visit(QJsonValue(obj), false);
    printf("%d\n%d\n", v.ans, v.ans1);
    return 0;
}
