#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

constexpr int m = 150;

int main() {
    vector<int> a(m + 1, INT_MAX >> 1), b(m + 1), c(m + 1);
    a[0] = 0;
    b[0] = 1;
    c[0] = 1;
    int k;
    while (scanf("%d", &k) == 1) {
        for (int i = m; i >= k; --i) {
            int t = a[i - k] + 1;
            if (t < a[i]) {
                a[i] = t;
                b[i] = b[i - k];
            } else if (t == a[i]) b[i] += b[i - k];
            c[i] += c[i - k];
        }
    }
    printf("%d\n%d\n", c[m], b[m]);
    return 0;
}
