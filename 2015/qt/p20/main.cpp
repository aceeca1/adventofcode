#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

struct Solution {
    int n;

    int findWithin(int m, int c0, int t) {
        vector<int> a(m + 1);
        for (int i = 1; i <= m; ++i) {
            int c = c0;
            for (int j = i; j <= m; j += i) {
                if (!c--) break;
                a[j] += i;
            }
            if (a[i] * t >= n) return i;
        }
        return -1;
    }

    void solve(int c0, int t) {
        int m = 1024;
        for (;;) {
            int a = findWithin(m, c0, t);
            if (a != -1) { m = a; break; }
            m += m;
        }
        printf("%d\n", m);
    }

    Solution() {
        scanf("%d", &n);
        solve(INT_MAX, 10);
        solve(50, 11);
    }
};

int main() {
    Solution();
    return 0;
}
