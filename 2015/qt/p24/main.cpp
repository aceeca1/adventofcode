#include <cstdio>
#include <climits>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Solution {
    vector<int> a;
    int k, s0 = 0, s, mLen;
    double mQu;

    void put(int m, int s1, int s2, int s3, int s4, int len, double qu) {
        int lenP = m == -1 ? len : len + (s - s1 + a[m] - 1) / a[m];
        if (lenP > mLen || (lenP == mLen && qu >= mQu)) return;
        if (m == -1) { mLen = lenP; mQu = qu; return; }
        if (s1 + a[m] <= s)
            put(m - 1, s1 + a[m], s2, s3, s4, len + 1, qu * a[m]);
        if (s2 + a[m] <= s)
            put(m - 1, s1, s2 + a[m], s3, s4, len, qu);
        if (s3 + a[m] <= s && s2)
            put(m - 1, s1, s2, s3 + a[m], s4, len, qu);
        if (k == 4 && s4 + a[m] <= s && s2 && s3)
            put(m - 1, s1, s2, s3, s4 + a[m], len, qu);
    }

    void solve() {
        s = s0 / k;
        mLen = INT_MAX;
        mQu = 1.0 / 0.0;
        put(a.size() - 1, 0, 0, 0, 0, 0, 1.0);
        printf("%.0f\n", mQu);
    }

    Solution() {
        int t;
        while (scanf("%d", &t) == 1) {
            a.emplace_back(t);
            s0 += t;
        }
        k = 3; solve();
        k = 4; solve();
    }
};

int main() {
    Solution();
    return 0;
}
