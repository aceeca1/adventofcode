#include <cstdio>
#include <memory>
using namespace std;

enum Split { X_SPLIT, Y_SPLIT, NO_SPLIT };

struct Rect {
    int x1, x2, y1, y2;
    void set(int x1_, int x2_, int y1_, int y2_) {
        x1 = x1_; x2 = x2_; y1 = y1_; y2 = y2_;
    }
    void split(Split dir, int at, Rect& r1, Rect& r2) const {
        switch (dir) {
            case X_SPLIT:
                r1.set(x1, at, y1, y2); r2.set(at, x2, y1, y2); break;
            case Y_SPLIT:
                r1.set(x1, x2, y1, at); r2.set(x1, x2, at, y2); break;
            default: break;
        }
    }
    bool excludes(const Rect& v) const {
        return (x2 <= v.x1 || v.x2 <= x1) || (y2 <= v.y1 || v.y2 <= y1);
    }
    bool contains(const Rect& v) const {
        return (x1 <= v.x1 && v.x2 <= x2) && (y1 <= v.y1 && v.y2 <= y2);
    }
    int area() const { return (x2 - x1) * (y2 - y1); }
};

template <class TBox>
shared_ptr<TBox> put(
    const Rect& rectOp, const TBox& op,
    const Rect& rectBox, const shared_ptr<TBox>& box) {
    if (rectOp.excludes(rectBox) || &op == &*box) return box;
    if (rectOp.contains(rectBox)) return op.applyTo(box);
    if (box->dir != NO_SPLIT) {
        box->push();
        Rect r1, r2;
        rectBox.split(box->dir, box->at, r1, r2);
        box->c1 = put(rectOp, op, r1, box->c1);
        box->c2 = put(rectOp, op, r2, box->c2);
        return box;
    }
    auto ch = op.applyTo(box);
    if (rectBox.x1 < rectOp.x1 && rectOp.x1 < rectBox.x2)
        ch = make_shared<TBox>(X_SPLIT, rectOp.x1, box, ch);
    if (rectBox.y1 < rectOp.y1 && rectOp.y1 < rectBox.y2)
        ch = make_shared<TBox>(Y_SPLIT, rectOp.y1, box, ch);
    if (rectBox.x1 < rectOp.x2 && rectOp.x2 < rectBox.x2)
        ch = make_shared<TBox>(X_SPLIT, rectOp.x2, ch, box);
    if (rectBox.y1 < rectOp.y2 && rectOp.y2 < rectBox.y2)
        ch = make_shared<TBox>(Y_SPLIT, rectOp.y2, ch, box);
    return ch;
}

template <class TBox>
int sum(const Rect& rect, TBox& box) {
    if (box.dir == NO_SPLIT) return box.sum(rect.area());
    box.push();
    Rect r1, r2;
    rect.split(box.dir, box.at, r1, r2);
    return sum(r1, *box.c1) + sum(r2, *box.c2);
}

template <class TBox>
struct Box {
    shared_ptr<TBox> c1, c2;
    int at;
    Split dir;
};

struct PowerBox: public Box<PowerBox> {
    bool toggled;
    PowerBox() { dir = NO_SPLIT; }
    PowerBox(
        Split dir_, int at_,
        shared_ptr<PowerBox> c1_, shared_ptr<PowerBox> c2_,
        bool toggled_ = false): toggled(toggled_) {
        c1 = move(c1_); c2 = move(c2_); at = at_; dir = dir_;
    }
    static shared_ptr<PowerBox>& on() {
        static auto instance = make_shared<PowerBox>();
        return instance;
    }
    static shared_ptr<PowerBox>& off() {
        static auto instance = make_shared<PowerBox>();
        return instance;
    }
    static shared_ptr<PowerBox>& toggle() {
        static auto instance = make_shared<PowerBox>();
        return instance;
    }
    shared_ptr<PowerBox> toggleBox() const {
        if (this == &*on()) return off();
        if (this == &*off()) return on();
        return make_shared<PowerBox>(dir, at, c1, c2, !toggled);
    }
    shared_ptr<PowerBox> applyTo(const shared_ptr<PowerBox>& v) const {
        if (this == &*on()) return on();
        if (this == &*off()) return off();
        return v->toggleBox();
    }
    void push() {
        if (!toggled) return;
        toggled = false;
        c1 = c1->toggleBox();
        c2 = c2->toggleBox();
    }
    int sum(int v) const { return this == &*on() ? v : 0; }
};

struct LightBox: public Box<LightBox> {
    int a, b;
    LightBox(Split dir_, int at_,
        shared_ptr<LightBox> c1_, shared_ptr<LightBox> c2_): a(0), b(0) {
        c1 = move(c1_); c2 = move(c2_); at = at_; dir = dir_;
    }
    LightBox(int a_, int b_): a(a_), b(b_) {}
    void set(int a_, int b_) { a = a_; b = b_; }
    shared_ptr<LightBox> applyTo(const shared_ptr<LightBox>& v) const {
        auto ans = make_shared<LightBox>(*v);
        ans->set(v->a + a, max(b, v->b + a));
        return ans;
    }
    void push() { c1 = applyTo(c1); c2 = applyTo(c2); a = b = 0; }
    int sum(int v) const { return v * max(a, b); }
};

Rect parse(const char* s) {
    Rect ans;
    sscanf(s, "%d,%d through %d,%d", &ans.x1, &ans.y1, &ans.x2, &ans.y2);
    return ++ans.x2, ++ans.y2, ans;
}

int main() {
    constexpr Rect root{0, 1000, 0, 1000};
    auto rPower = PowerBox::off();
    auto rLight = make_shared<LightBox>(NO_SPLIT, 0, nullptr, nullptr);
    char s[34];
    while (fgets(s, 34, stdin)) {
        if (equal(s, s + 7, "turn on")) {
            auto rect = parse(s + 8);
            rPower = put(rect, *PowerBox::on(),      root, rPower);
            rLight = put(rect, LightBox(1,  0),      root, rLight);
        } else if (equal(s, s + 8, "turn off")) {
            auto rect = parse(s + 9);
            rPower = put(rect, *PowerBox::off(),     root, rPower);
            rLight = put(rect, LightBox(-1, 0),      root, rLight);
        } else {
            auto rect = parse(s + 7);
            rPower = put(rect, *PowerBox::toggle(),  root, rPower);
            rLight = put(rect, LightBox(2,  0),      root, rLight);
        }
    }
    printf("%d\n", sum(root, *rPower));
    printf("%d\n", sum(root, *rLight));
    return 0;
}
