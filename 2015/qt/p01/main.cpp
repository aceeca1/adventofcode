#include <cstdio>
using namespace std;

int main() {
    char s[7700];
    scanf("%s", s);
    int ans = 0;
    for (int i = 0; s[i]; ++i) switch (s[i]) {
        case '(': ++ans; break;
        case ')': --ans;
    }
    printf("%d\n", ans);
    ans = 0;
    int i = 0;
    while (ans >= 0) switch (s[i++]) {
        case '(': ++ans; break;
        case ')': --ans;
    }
    printf("%d\n", i);
    return 0;
}
