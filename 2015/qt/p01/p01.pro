TEMPLATE = app
TARGET = main
INCLUDEPATH += .
QT -= gui
SOURCES += $$files(*.cpp)
QMAKE_CXXFLAGS += -std=c++17 -Wall -Wextra -Werror -g -Og
