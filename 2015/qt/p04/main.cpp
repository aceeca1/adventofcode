#include <cstdio>
#include <QCryptographicHash>
using namespace std;

template <int k>
bool is(string s, int n) {
    s += to_string(n);
    QCryptographicHash qch(QCryptographicHash::Md5);
    qch.addData(s.c_str(), s.size());
    const auto& a = qch.result();
    if (k == 5) return !a[0] && !a[1] && !(a[2] & 0xf0);
    if (k == 6) return !a[0] && !a[1] && !a[2];
    return 0;
}

int main() {
    char s[11];
    scanf("%s", s);
    string ss(s);
    int i5 = 1, i6 = 1;
    while (!is<5>(ss, i5)) ++i5;
    printf("%d\n", i5);
    while (!is<6>(ss, i6)) ++i6;
    printf("%d\n", i6);
    return 0;
}
