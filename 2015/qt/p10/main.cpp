#include <cstdio>
#include <string>
using namespace std;

string lookNSay(string s) {
    string ret;
    char u = '~';
    int times = 0;
    for (int i = 0; i <= (int)s.size(); ++i)
        if (i == (int)s.size() || s[i] != u) {
            if (times) (ret += to_string(times)) += u;
            u = s[i]; times = 1;
        } else ++times;
    return ret;
}

int main() {
    char s[11];
    scanf("%s", s);
    string ss(s);
    for (int i = 0; i < 40; ++i) ss = lookNSay(ss);
    printf("%d\n", (int)ss.size());
    for (int i = 40; i < 50; ++i) ss = lookNSay(ss);
    printf("%d\n", (int)ss.size());
    return 0;
}
