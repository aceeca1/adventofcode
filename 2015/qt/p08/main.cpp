#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    char s[81];
    int ans1 = 0, ans2 = 0;
    while (scanf("%s", s) == 1) {
        int z = strlen(s);
        ans1 += z + 2;
        for (int i = 0; s[i];)
            if (s[i] == '\\') switch (s[i + 1]) {
                case '\\':  --ans1; i += 2; break;
                case '"':   --ans1; i += 2; break;
                case 'x':   --ans1; i += 4; break;
            }
            else --ans1, ++i;
        ans2 += 2 - z;
        for (int i = 0; s[i]; ++i) switch (s[i]) {
            case '\\':  ans2 += 2; break;
            case '"':   ans2 += 2; break;
            default:    ++ans2;
        }
    }
    printf("%d\n%d\n", ans1, ans2);
    return 0;
}
