let file = new System.IO.StreamReader("../data/input04.txt")
let read = file.ReadLine
let mutable ans1 = 0
let mutable ans2 = 0
let n = 991
for i = 0 to n - 1 do
    let s = read()
    let sep2 = s.IndexOf('[')
    let mutable sep1 = sep2
    while s.[sep1] <> '-' do sep1 <- sep1 - 1
    let a = Array.zeroCreate<int>(256)
    for i = 0 to sep1 - 1 do
        if System.Char.IsLower(s.[i]) then
            a.[int s.[i]] <- a.[int s.[i]] + 1
    let b = (seq {'a' .. 'z'}) |> Seq.sortBy(fun i -> -a.[int i], i)
    let check = b |> Seq.take(5) |> Seq.toArray |> System.String
    let checkA = s.[sep2 + 1 .. s.Length - 2]
    let id = int s.[sep1 + 1 .. sep2 - 1]
    if check = checkA then ans1 <- ans1 + id
    let decrypt = Array.init(sep1)(fun i ->
        if System.Char.IsLower(s.[i]) |> not then s.[i] else
            (int s.[i] - int 'a' + id) % 26 + int 'a' |> char)
    if System.String(decrypt) = "northpole-object-storage" then ans2 <- id
ans1 |> printfn "%d"
ans2 |> printfn "%d"
