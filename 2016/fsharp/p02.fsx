let file = new System.IO.StreamReader("../data/input02.txt")
let read = file.ReadLine
let mutable c = 5
let a = Array.init(5)(fun i -> read())
System.String.Concat(a |> Seq.map(fun ai ->
    for i in ai do
        match i with
        | 'R' -> c <- [|0; 2; 3; 3; 5; 6; 6; 8; 9; 9|].[c]
        | 'U' -> c <- [|0; 1; 2; 3; 1; 2; 3; 4; 5; 6|].[c]
        | 'L' -> c <- [|0; 1; 1; 2; 4; 4; 5; 7; 7; 8|].[c]
        | 'D' -> c <- [|0; 4; 5; 6; 7; 8; 9; 7; 8; 9|].[c]
        | _ -> failwith "bad input"
    c)) |> printfn "%s"
let mutable e = '5'
let orig = "123456789ABCD"
let orig2 = Array.zeroCreate<int>(256)
for i = 0 to orig.Length - 1 do orig2.[int orig.[i]] <- i
System.String.Concat(a |> Seq.map(fun ai ->
    for i in ai do
        match i with
        | 'R' -> e <- "134467899BCCD".[orig2.[int e]]
        | 'U' -> e <- "121452349678B".[orig2.[int e]]
        | 'L' -> e <- "122355678AABD".[orig2.[int e]]
        | 'D' -> e <- "36785ABC9ADCD".[orig2.[int e]]
        | _ -> failwith "bad input"
    e)) |> printfn "%s"
