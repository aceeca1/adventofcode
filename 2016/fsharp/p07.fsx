open System.Linq
let abba(s: string) =
    (seq {0 .. s.Length - 4}) |> Seq.exists(fun i ->
        s.[i] <> s.[i + 1] && s.[i] = s.[i + 3] && s.[i + 1] = s.[i + 2])
let aba(s: string) = seq {
    for i = 0 to s.Length - 3 do
        if s.[i] <> s.[i + 1] && s.[i] = s.[i + 2] then
            yield (s.[i], s.[i + 1])
}
let file = new System.IO.StreamReader("../data/input07.txt")
let read = file.ReadLine
let re = System.Text.RegularExpressions.Regex("\[.*?\]")
let mutable ans1, ans2 = 0, 0
type DCCI = System.Collections.Generic.Dictionary<char * char, int>
let addToDCCI(dcci: DCCI, v: char * char) =
    if dcci.ContainsKey(v) then dcci.[v] <- dcci.[v] + 1
    else dcci.Add(v, 1) |> ignore
for i = 1 to 2000 do
    let s = read()
    let m = re.Matches(s).Cast<System.Text.RegularExpressions.Match>()
    let ban = m |> Seq.exists(fun i -> abba(i.Value))
    if not ban && abba(s) then ans1 <- ans1 + 1
    let m1 = System.Collections.Generic.Dictionary<char * char, int>()
    for i in aba(s) do addToDCCI(m1, i)
    let m2 = System.Collections.Generic.Dictionary<char * char, int>()
    for i in m do for j in aba(i.Value) do addToDCCI(m2, j)
    for i in m2 do m1.[i.Key] <- m1.[i.Key] - i.Value
    if m1 |> Seq.exists(fun i ->
        i.Value <> 0 && m2.ContainsKey(snd i.Key, fst i.Key)) then
        ans2 <- ans2 + 1
ans1 |> printfn "%d"
ans2 |> printfn "%d"
