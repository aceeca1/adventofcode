type Node(s: string) =
    let reS = "/dev/grid/node-x(\d+)-y(\d+) +\d+T +(\d+)T +(\d+)T.*"
    let re = System.Text.RegularExpressions.Regex(reS)
    let g = re.Match(s).Groups
    member val X = int g.[1].Value
    member val Y = int g.[2].Value
    member val Used = int g.[3].Value
    member val Avail = int g.[4].Value
let file = new System.IO.StreamReader("../data/input22.txt")
let read = file.ReadLine
read() |> ignore
read() |> ignore
let a = Array.init(1054)(fun i -> read() |> Node)
let mutable ans = 0
for i in a do
    for j in a do
        if i.Used <> 0 && i <> j && i.Used <= j.Avail then
            ans <- ans + 1
ans |> printfn "%d"
(* Draw a map and calculate by hand, use the following technique:
v---<
|   |
*   ^
    |
>---^ *)
let dis(sX, sY, tX, tY) = abs(tX - sX) + abs(tY - sY)
dis(20, 25, 4, 0) + dis(4, 0, 33, 0) + 32 * 5 |> printfn "%d"
