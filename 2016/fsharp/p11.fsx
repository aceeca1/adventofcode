let h128to64(u1, u2) =
    let kMul = 0x9ddfea08eb382d69L
    let a1 = (u1 ^^^ u2) * kMul
    let a2 = a1 ^^^ (a1 >>> 47)
    let b1 = (a2 ^^^ u1) * kMul
    b1 ^^^ (b1 >>> 47)
type Node(a: int[], e, n) =
    override u.GetHashCode() =
        let mutable h = 0L
        for i in 0 .. 2 .. a.Length - 1 do
            h <- h + h128to64(int64 a.[i], int64 a.[i + 1])
        int h + e
    override u1.Equals(u2) = hash u1 = hash u2
    (* For Debug *)
    member u.Print() =
        System.Console.WriteLine()
        System.Console.WriteLine(
            "Elevator: {0}, Steps: {1}, Prio: {2}", e, n, u.Prio)
        for i in a do System.Console.Write("{0} ", i)
        System.Console.WriteLine()
    member u.N = n
    member u.Finish = a |> Array.forall(fun i -> i = 3)
    member u.Valid =
        (seq {0 .. 2 .. a.Length - 1}) |> Seq.forall(fun i ->
            a.[i] = a.[i + 1] ||
            (seq {1 .. 2 .. a.Length - 1})
            |> Seq.forall(fun j -> a.[i] <> a.[j]))
    member u.Child1(eN, i1) =
        let aN = a.Clone() :?> int[]
        aN.[i1] <- eN
        Node(aN, eN, n + 1)
    member u.Child2(eN, i1, i2) =
        let aN = a.Clone() :?> int[]
        aN.[i1] <- eN
        aN.[i2] <- eN
        Node(aN, eN, n + 1)
    member u.Childs =
        (seq {
            for eN in [|e - 1; e + 1|] do
                if 0 <= eN && eN < 4 then
                    for i = 0 to a.Length - 1 do
                        if a.[i] = e then
                            yield u.Child1(eN, i)
                            for j = i + 1 to a.Length - 1 do
                                if a.[j] = e then yield u.Child2(eN, i, j)
        }) |> Seq.filter(fun i -> i.Valid)
    member val Prio = n + (a |> Seq.sumBy(fun i -> 3 - i)) - (3 - e)
    interface System.IComparable<Node> with
        member u1.CompareTo(u2) =
            compare (u1.Prio, hash u1) (u2.Prio, hash u2)
let solve(a) =
    let q = System.Collections.Generic.SortedSet<Node>()
    let v = System.Collections.Generic.HashSet<int>()
    let start = Node(a, 0, 0)
    q.Add(start) |> ignore
    let mutable ans = 0
    try
        while q.Count <> 0 do
            let qH = q.Min
            q.Remove(qH) |> ignore
            let qHH = hash qH
            if v.Contains(qHH) |> not then
                v.Add(qHH) |> ignore
                if qH.Finish then
                    ans <- qH.N
                    failwith "found"
                for i in qH.Childs do q.Add(i) |> ignore
    with _ -> ans |> printfn "%d"
let file = new System.IO.StreamReader("../data/input11.txt")
let read = file.ReadLine
let a = System.Collections.Generic.Dictionary<string, int[]>()
for e = 0 to 3 do
    let s = read().Split() |> Array.filter(fun i -> i <> "and")
    if s.[4] <> "nothing" then
        for i in 4 .. 3 .. s.Length - 1 do
            let si = s.[i + 1].Split('-')
            if a.ContainsKey(si.[0]) |> not then
                a.Add(si.[0], Array.zeroCreate<int>(2))
            if si.Length = 1 then a.[si.[0]].[1] <- e
            else a.[si.[0]].[0] <- e
let b = [| for i in a do for j in i.Value -> j |]
solve(b)
solve([| yield! b; yield! [|0; 0; 0; 0|] |])
