let file = new System.IO.StreamReader("../data/input24.txt")
let read = file.ReadLine
let vSize = 43
let hSize = 181
let a = Array.init(vSize)(fun i -> read())
let mark = Array.zeroCreate<int * int>(8)
for i = 0 to vSize - 1 do
    for j = 0 to hSize - 1 do
        if System.Char.IsDigit(a.[i].[j]) then
            mark.[int a.[i].[j] - int '0'] <- i, j
let dis = Array2D.zeroCreate<int>(mark.Length)(mark.Length)
for i = 0 to mark.Length - 1 do
    let d = Array2D.init(vSize)(hSize)(fun i j -> System.Int32.MaxValue)
    let startX, startY = mark.[i]
    d.[startX, startY] <- 0
    let q = System.Collections.Generic.Queue<int * int>()
    q.Enqueue(startX, startY)
    while q.Count <> 0 do
        let qHX, qHY = q.Dequeue()
        let u = [|qHX, qHY + 1; qHX - 1, qHY; qHX, qHY - 1; qHX + 1, qHY|]
        for nX, nY in u do
            if a.[nX].[nY] <> '#' then
                let dN = d.[qHX, qHY] + 1
                if dN < d.[nX, nY] then
                    d.[nX, nY] <- dN
                    q.Enqueue(nX, nY)
    for j = 0 to mark.Length - 1 do
        let endX, endY = mark.[j]
        dis.[i, j] <- d.[endX, endY]
let mutable ans1 = System.Int32.MaxValue
let mutable ans2 = System.Int32.MaxValue
let b = [|0 .. mark.Length - 1|]
let rec f(m, len) =
    if m = mark.Length then
        if len < ans1 then ans1 <- len
        let len2 = len + dis.[b.[0], b.[m - 1]]
        if len2 < ans2 then ans2 <- len2
    else
        let bm = b.[m]
        for i = mark.Length - 1 downto m do
            b.[m] <- b.[i]
            b.[i] <- bm
            f(m + 1, len + dis.[b.[m], b.[m - 1]])
            b.[i] <- b.[m]
f(1, 0)
ans1 |> printfn "%d"
ans2 |> printfn "%d"
