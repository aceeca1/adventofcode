let file = new System.IO.StreamReader("../data/input17.txt")
let read = file.ReadLine
let s = read()
let md5er = System.Security.Cryptography.MD5.Create()
let md5(s: string) =
    s |> System.Text.Encoding.ASCII.GetBytes |> md5er.ComputeHash
let doors(path: string) =
    let m = md5(s + path)
    [| m.[0] >>> 4; m.[0] &&& 0xfuy; m.[1] >>> 4; m.[1] &&& 0xfuy |]
let dx = [|-1; 1;  0; 0|]
let dy = [| 0; 0; -1; 1|]
type Node(x, y, path) =
    member u.Path = path
    member u.Finish = x = 3 && y = 3
    member u.Childs = seq {
        let d = doors(path)
        for i = 0 to 3 do
            if d.[i] > 10uy then
                let xN = x + dx.[i]
                let yN = y + dy.[i]
                if 0 <= xN && xN < 4 && 0 <= yN && yN < 4 then
                    yield Node(xN, yN, path + string "UDLR".[i])
    }
let mutable ans1 = System.Int32.MaxValue
let mutable iAns1 = null: string
let mutable ans2 = 0
let rec visit(n: Node) =
    if n.Finish then
        if n.Path.Length < ans1 then
            ans1 <- n.Path.Length
            iAns1 <- n.Path
        if n.Path.Length > ans2 then
            ans2 <- n.Path.Length
    else for i in n.Childs do visit(i)
visit(Node(0, 0, ""))
iAns1 |> printfn "%s"
ans2 |> printfn "%d"
