let dragon(a: string) =
    let b = a.ToCharArray()
    System.Array.Reverse(b)
    for i = 0 to b.Length - 1 do b.[i] <- char(int b.[i] ^^^ 1)
    a + "0" + System.String(b)
let rec dragonUntil(a: string, n) =
    if a.Length >= n then a.[.. n - 1] else dragonUntil(dragon(a), n)
let rec checksum(a: string) =
    if a.Length &&& 1 <> 0 then a else
        Array.init(a.Length >>> 1)(fun i ->
            if a.[i + i] = a.[i + i + 1] then '1' else '0')
        |> System.String |> checksum
let file = new System.IO.StreamReader("../data/input16.txt")
let read = file.ReadLine
let s = read()
dragonUntil(s, 272) |> checksum |> printfn "%s"
dragonUntil(s, 35651584) |> checksum |> printfn "%s"
