let file = new System.IO.StreamReader("../data/input05.txt")
let read = file.ReadLine
let s = read()
let md5er = System.Security.Cryptography.MD5.Create()
let md5(s: string) =
    s |> System.Text.Encoding.ASCII.GetBytes |> md5er.ComputeHash
let ans1 = System.Text.StringBuilder()
let ans2 = Array.zeroCreate<char>(8)
let mutable z2 = 0
let mutable i = 0
let zero5(b: byte[]) = b.[0] = 0uy && b.[1] = 0uy && (b.[2] &&& 0xf0uy = 0uy)
while ans1.Length < 8 || z2 < 8 do
    while md5(s + string i) |> zero5 |> not do i <- i + 1
    let m = md5(s + string i)
    let m6 = m.[2] &&& 0xfuy
    let m7 = m.[3] >>> 4
    if ans1.Length < 8 then ans1.Append(m6 |> sprintf "%x") |> ignore
    if m6 < 8uy && ans2.[int m6] = char 0 then
        ans2.[int m6] <- (m7 |> sprintf "%x").[0]
        z2 <- z2 + 1
    i <- i + 1
ans1.ToString() |> printfn "%s"
System.String(ans2) |> printfn "%s"
