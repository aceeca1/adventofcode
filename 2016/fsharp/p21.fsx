type Password(s: string) =
    let mutable pa = s.ToCharArray()
    let swapP(x, y) =
        let paX = pa.[x]
        pa.[x] <- pa.[y]
        pa.[y] <- paX
    let idx(c) = System.Array.IndexOf(pa, c)
    let swapL(x, y) = swapP(idx(x), idx(y))
    let rotateR(x) =
        pa <- Array.init(pa.Length)(fun i ->
            pa.[(i - x + pa.Length) % pa.Length])
    let rotateL(x) = rotateR(pa.Length - x)
    let rotBLen(ix) = 1 + ix + (if ix >= 4 then 1 else 0)
    let rotateB(x) = rotateR(rotBLen(idx(x)) % pa.Length)
    let reverse(x, y) = System.Array.Reverse(pa, x, y - x + 1)
    let move(x, y) =
        let paX = pa.[x]
        if x < y then
            for i = x to y - 1 do pa.[i] <- pa.[i + 1]
            pa.[y] <- paX
        else
            for i = x downto y + 1 do pa.[i] <- pa.[i - 1]
            pa.[y] <- paX
    let rotateBR(x) =
        let iy = idx(x)
        let ix = (seq {0 .. pa.Length - 1}) |> Seq.findIndex(fun ix ->
            (ix + rotBLen(ix)) % pa.Length = iy)
        rotateL(rotBLen(ix) % pa.Length)
    member u.Pa = System.String(pa)
    member u.Op(op: string) =
        let ops = op.Split()
        match ops.[0] with
        | "swap" ->
            match ops.[1] with
            | "position" -> swapP(int ops.[2], int ops.[5])
            | "letter" -> swapL(ops.[2].[0], ops.[5].[0])
            | _ -> failwith "bad input"
        | "rotate" ->
            match ops.[1] with
            | "left" -> rotateL(int ops.[2])
            | "right" -> rotateR(int ops.[2])
            | "based" -> rotateB(ops.[6].[0])
            | _ -> failwith "bad input"
        | "reverse" -> reverse(int ops.[2], int ops.[4])
        | "move" -> move(int ops.[2], int ops.[5])
        | _ -> failwith "bad input"
    member u.OpR(op: string) =
        let ops = op.Split()
        match ops.[0] with
        | "swap" ->
            match ops.[1] with
            | "position" -> swapP(int ops.[2], int ops.[5])
            | "letter" -> swapL(ops.[2].[0], ops.[5].[0])
            | _ -> failwith "bad input"
        | "rotate" ->
            match ops.[1] with
            | "left" -> rotateR(int ops.[2])
            | "right" -> rotateL(int ops.[2])
            | "based" -> rotateBR(ops.[6].[0])
            | _ -> failwith "bad input"
        | "reverse" -> reverse(int ops.[2], int ops.[4])
        | "move" -> move(int ops.[5], int ops.[2])
        | _ -> failwith "bad input"
let file = new System.IO.StreamReader("../data/input21.txt")
let read = file.ReadLine
let a = Array.init(100)(fun i -> read())
let pass = Password("abcdefgh")
for i in a do pass.Op(i)
pass.Pa |> printfn "%s"
let pass2 = Password("fbgdceah")
for i = a.Length - 1 downto 0 do pass2.OpR(a.[i])
pass2.Pa |> printfn "%s"
