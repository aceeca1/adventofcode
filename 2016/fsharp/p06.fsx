let file = new System.IO.StreamReader("../data/input06.txt")
let read = file.ReadLine
let a = Array.init(624)(fun i -> read())
let ans = Array.init(8)(fun i ->
    let c = Array.zeroCreate<int>(256)
    for ai in a do c.[int ai.[i]] <- c.[int ai.[i]] + 1
    (seq {'a' .. 'z'}) |> Seq.maxBy(fun i -> c.[int i]),
    (seq {'a' .. 'z'}) |> Seq.minBy(fun i ->
        if c.[int i] = 0 then System.Int32.MaxValue else c.[int i]))
System.String(ans |> Array.map(fst)) |> printfn "%s"
System.String(ans |> Array.map(snd)) |> printfn "%s"
