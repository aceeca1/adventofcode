let wide = 50
let tall = 6
type Screen() =
    let a = Array2D.zeroCreate<bool>(wide)(tall)
    member u.Op(s: string) =
        let ss = s.Split()
        match ss.[0] with
        | "rect" ->
            let ss1s = ss.[1].Split('x')
            for i = 0 to int ss1s.[0] - 1 do
                for j = 0 to int ss1s.[1] - 1 do
                    a.[i, j] <- true
        | "rotate" ->
            let no = ss.[2].Split('=').[1] |> int
            let offset = int ss.[4]
            match ss.[1] with
            | "row" ->
                let b = Array.init(wide)(fun i -> a.[i, no])
                for i = 0 to wide - 1 do
                    a.[(i + offset) % wide, no] <- b.[i]
            | "column" ->
                let b = Array.init(tall)(fun i -> a.[no, i])
                for i = 0 to tall - 1 do
                    a.[no, (i + offset) % tall] <- b.[i]
            | _ -> failwith "bad input"
        | _ -> failwith "bad input"
    member u.Sum() =
        let mutable ret = 0
        for i = 0 to wide - 1 do
            for j = 0 to tall - 1 do
                if a.[i, j] then ret <- ret + 1
        ret
    member u.Display() =
        for i = 0 to tall - 1 do
            for j = 0 to wide - 1 do
                printf "%c" (if a.[j, i] then '#' else '.')
            printfn ""
let file = new System.IO.StreamReader("../data/input08.txt")
let read = file.ReadLine
let sc = Screen()
for i = 1 to 162 do sc.Op(read())
sc.Sum() |> printfn "%d"
sc.Display()
