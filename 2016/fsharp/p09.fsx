let rec decomp(s: string, recur) =
    let mutable ret = 0L
    let mutable i = 0
    while i < s.Length do
        if s.[i] = '(' then
            let iM = s.IndexOf('x', i + 1)
            let iR = s.IndexOf(')', iM + 1)
            let len = int s.[i + 1 .. iM - 1]
            let times = int64 s.[iM + 1 .. iR - 1]
            let ss = s.[iR + 1 .. iR + len]
            let ssLen = if recur then decomp(ss, recur) else int64 ss.Length
            ret <- ret + times * ssLen
            i <- iR + len + 1
        else
            ret <- ret + 1L
            i <- i + 1
    ret
let file = new System.IO.StreamReader("../data/input09.txt")
let read = file.ReadLine
let s = read()
decomp(s, false) |> printfn "%d"
decomp(s, true) |> printfn "%d"
