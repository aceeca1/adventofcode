open System.Linq
let h128to64(u1, u2) =
    let kMul = 0x9ddfea08eb382d69L
    let a1 = (u1 ^^^ u2) * kMul
    let a2 = a1 ^^^ (a1 >>> 47)
    let b1 = (a2 ^^^ u1) * kMul
    b1 ^^^ (b1 >>> 47)
let h64(u) = h128to64(u, 0L)
type HSS = System.Collections.Generic.HashSet<string>
let canStay(m: string, fl: HSS) =
    let g = m.[.. m.Length - 2] + "G"
    fl.Contains(g) ||
    fl |> Seq.forall(fun i -> i.EndsWith("M"))
type Node(a: HSS[], e, n) =
    override u.GetHashCode() =
        (a |> Array.map(fun i ->
            let mutable h = 0L
            for j in i do h <- h + h64(hash j |> int64)
            h) |> hash) + (h64(e |> int64) |> int)
    override u1.Equals(u2) = hash u1 = hash u2
    (* For Debug *)
    member u.Print() =
        System.Console.WriteLine()
        System.Console.WriteLine(
            "Elevator: {0}, Steps: {1}, Prio: {2}", e, n, u.Prio)
        for ai in a do
            for j in ai do System.Console.Write("{0} ", j)
            System.Console.WriteLine()
    member u.N = n
    member u.Finish = (seq {0 .. 2}) |> Seq.forall(fun i -> a.[i].Count = 0)
    member u.Valid =
        (seq {
            for ai in a do
                for m in ai do
                    if m.EndsWith("M") then
                        yield canStay(m, ai)
        }) |> Seq.forall(id)
    member u.Child1(eN, aeA1) =
        let aN = Array.init(4)(fun i -> HSS(a.[i]))
        aN.[e].Remove(aeA1) |> ignore
        aN.[eN].Add(aeA1) |> ignore
        Node(aN, eN, n + 1)
    member u.Child2(eN, aeA1, aeA2) =
        let aN = Array.init(4)(fun i -> HSS(a.[i]))
        aN.[e].Remove(aeA1) |> ignore
        aN.[e].Remove(aeA2) |> ignore
        aN.[eN].Add(aeA1) |> ignore
        aN.[eN].Add(aeA2) |> ignore
        Node(aN, eN, n + 1)
    member u.Childs =
        (seq {
            let aeA = a.[e].ToArray()
            for eN in [|e - 1; e + 1|] do
                if 0 <= eN && eN < 4 then
                    for i = 0 to aeA.Length - 1 do
                        yield u.Child1(eN, aeA.[i])
                    for i = 0 to aeA.Length - 1 do
                        for j = i + 1 to aeA.Length - 1 do
                            yield u.Child2(eN, aeA.[i], aeA.[j])
        }) |> Seq.filter(fun i -> i.Valid)
    member val Prio =
        n + a.[0].Count * 3 + a.[1].Count * 2 + a.[2].Count - (3 - e)
    interface System.IComparable<Node> with
        member u1.CompareTo(u2) =
            compare (u1.Prio, hash u1) (u2.Prio, hash u2)
let solve(a) =
    let q = System.Collections.Generic.SortedSet<Node>()
    let v = System.Collections.Generic.HashSet<int>()
    let start = Node(a, 0, 0)
    q.Add(start) |> ignore
    v.Add(hash start) |> ignore
    let mutable ans = 0
    try
        while q.Count <> 0 do
            let qH = q.Min
            q.Remove(qH) |> ignore
            for i in qH.Childs do
                let iH = hash i
                if v.Contains(iH) |> not then
                    v.Add(iH) |> ignore
                    q.Add(i) |> ignore
                    if i.Finish then
                        ans <- i.N
                        failwith "found"
    with _ -> ans |> printfn "%d"
let eleSym = [|
    "polonium",  "Po"; "thulium", "Tm"; "promethium", "Pm"
    "ruthenium", "Ru"; "cobalt",  "Co" |] |> dict
let file = new System.IO.StreamReader("../data/input11.txt")
let read = file.ReadLine
let a = Array.init(4)(fun i ->
    let s = read().Split() |> Array.filter(fun i -> i <> "and")
    if s.[4] = "nothing" then HSS()
    else
        let ret = HSS()
        for i in 4 .. 3 .. s.Length - 1 do
            let si = s.[i + 1].Split('-')
            if si.Length = 1 then ret.Add(eleSym.[si.[0]] + "G") |> ignore
            else ret.Add(eleSym.[si.[0]] + "M") |> ignore
        ret)
solve(a)
a.[0].Add("ElG") |> ignore
a.[0].Add("ElM") |> ignore
a.[0].Add("DiG") |> ignore
a.[0].Add("DiM") |> ignore
solve(a)
