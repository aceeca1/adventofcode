let file = new System.IO.StreamReader("../data/input13.txt")
let read = file.ReadLine
let c = read() |> int
let bitCount(n0) =
    let n1 = (n0 &&& 0x55555555) + ((n0 >>> 1) &&& 0x55555555)
    let n2 = (n1 &&& 0x33333333) + ((n1 >>> 2) &&& 0x33333333)
    let n3 = (n2 &&& 0x0f0f0f0f) + ((n2 >>> 4) &&& 0x0f0f0f0f)
    let n4 = (n3 &&& 0x00ff00ff) + ((n3 >>> 8) &&& 0x00ff00ff)
    (n4 &&& 0xffff) + (n4 >>> 16)
let block(x, y) =
    let a = x * x + 3 * x + 2 * x * y + y + y * y + c
    if bitCount(a) &&& 1 = 0 then '.' else '#'
let a = System.Collections.Generic.Dictionary<int * int, char>()
let blockA(x, y) =
    if x < 0 || y < 0 then '#' else
        if a.ContainsKey(x, y) |> not then a.[(x, y)] <- block(x, y)
        a.[x, y]
type Node(x, y, n) =
    override u.GetHashCode() = hash (x, y)
    override u1.Equals(u2) = hash u1 = hash u2
    (* For Debug *)
    member u.Print() = printfn "(%d, %d) N:%d" x y n
    member u.X = x
    member u.Y = y
    member u.N = n
    member u.Finish = x = 31 && y = 39
    member u.Valid = blockA(x, y) = '.'
    member u.Childs = seq {
        let w = [|x + 1, y; x, y + 1; x - 1, y; x, y - 1|]
        for xN, yN in w -> Node(xN, yN, n + 1) }
    member val Prio = abs(x - 31) + abs(y - 39)
    interface System.IComparable<Node> with
        member u1.CompareTo(u2) =
            compare (u1.Prio, hash u1) (u2.Prio, hash u2)
let q = System.Collections.Generic.SortedSet<Node>()
let start = Node(1, 1, 0)
q.Add(start) |> ignore
let mutable ans1 = 0
try
    while q.Count <> 0 do
        let qH = q.Min
        q.Remove(qH) |> ignore
        if qH.Valid then
            a.[(qH.X, qH.Y)] <- 'v'
            if qH.Finish then
                ans1 <- qH.N
                failwith "found"
            for i in qH.Childs do
                if i.Valid then q.Add(i) |> ignore
with _ -> ans1 |> printfn "%d"
let q2 = System.Collections.Generic.Queue<Node>()
q2.Enqueue(start)
a.[(start.X, start.Y)] <- 'w'
let mutable ans2 = 0
let isOpen(x, y) = let b = blockA(x, y) in b = '.' || b = 'v'
try
    while q2.Count <> 0 do
        let qH = q2.Dequeue()
        if qH.N > 50 then failwith "found"
        ans2 <- ans2 + 1
        for i in qH.Childs do
            if isOpen(i.X, i.Y) then
                a.[(i.X, i.Y)] <- 'w'
                q2.Enqueue(i)
with _ -> ans2 |> printfn "%d"
