let file = new System.IO.StreamReader("../data/input01.txt")
let read = file.ReadLine
let mutable x, y, dir = 0, 0, 0
let dx = [| 0; -1;  0;  1|]
let dy = [| 1;  0; -1;  0|]
let mutable repX, repY = System.Int32.MaxValue, 0
let a = System.Collections.Generic.HashSet<int * int>()
a.Add(0, 0) |> ignore
for i in read().Split([|", "|], System.StringSplitOptions.None) do
    match i.[0] with
    | 'L' -> dir <- (dir + 1) &&& 3
    | 'R' -> dir <- (dir + 3) &&& 3
    | _ -> failwith "bad input"
    for i = 1 to int i.[1 ..] do
        x <- x + dx.[dir]
        y <- y + dy.[dir]
        if repX = System.Int32.MaxValue && a.Contains(x, y) then
            repX <- x
            repY <- y
        else a.Add(x, y) |> ignore
let dis(x, y) = abs x + abs y
dis(x, y) |> printfn "%d"
dis(repX, repY) |> printfn "%d"
