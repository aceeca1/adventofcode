let file = new System.IO.StreamReader("../data/input23a.txt")
let read = file.ReadLine
let e0 = Array.init(26)(fun i -> read())
let solve(a) =
    let e = e0.Clone() :?> string[]
    let reg = System.Collections.Generic.Dictionary<string, int>()
    for i in [|"a"; "b"; "c"; "d"|] do reg.Add(i, 0) |> ignore
    reg.["a"] <- a
    let mutable c = 0
    let parse(s) =
        match System.Int32.TryParse(s) with
        | true, n -> n
        | _ -> reg.[s]
    let toggle(s: string) =
        let ss = s.Split()
        match ss.[0] with
        | "cpy" -> sprintf "jnz %s %s" ss.[1] ss.[2]
        | "inc" -> sprintf "dec %s" ss.[1]
        | "dec" -> sprintf "inc %s" ss.[1]
        | "jnz" -> sprintf "cpy %s %s" ss.[1] ss.[2]
        | "tgl" -> sprintf "inc %s" ss.[1]
        | _ -> failwith "bad logic"
    while c < e.Length do
        let ec = e.[c].Split()
        match ec.[0] with
        | "cpy" ->
            if reg.ContainsKey(ec.[2]) then
                reg.[ec.[2]] <- parse(ec.[1])
            c <- c + 1
        | "inc" ->
            if reg.ContainsKey(ec.[1]) then
                reg.[ec.[1]] <- reg.[ec.[1]] + 1
            c <- c + 1
        | "dec" ->
            if reg.ContainsKey(ec.[1]) then
                reg.[ec.[1]] <- reg.[ec.[1]] - 1
            c <- c + 1
        | "mul" ->
            if reg.ContainsKey(ec.[2]) then
                reg.[ec.[2]] <- reg.[ec.[2]] * parse(ec.[1])
            c <- c + 1
        | "jnz" ->
            c <- if parse(ec.[1]) = 0 then c + 1 else c + parse(ec.[2])
        | "tgl" ->
            let p = c + parse(ec.[1])
            if 0 <= p && p < e.Length then
                let tg = toggle(e.[p])
                //printfn "%d: '%s' -> '%s'" p e.[p] tg
                e.[p] <- tg
            c <- c + 1
        | _ -> failwith "bad logic"
    reg.["a"] |> printfn "%d"
solve(7)
solve(12)
