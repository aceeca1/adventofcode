let file = new System.IO.StreamReader("../data/input20.txt")
let read = file.ReadLine
let a = Array.init(1075)(fun i -> read().Split('-') |> Array.map(uint32))
System.Array.Sort(a, {new System.Collections.Generic.IComparer<uint32[]> with
    member u.Compare(a1, a2) = compare a1.[0] a2.[0]
})
let b = [|
    let mutable c = a.[0]
    for i = 1 to a.Length - 1 do
        if a.[i].[0] - 1u <= c.[1] then
            c.[1] <- max(c.[1])(a.[i].[1])
        else
            yield c
            c <- a.[i]
    yield c |]
let ans1 = if b.[0].[0] <> 0u then 0u else b.[0].[1] + 1u
ans1 |> printfn "%d"
let ban = b |> Array.sumBy(fun i -> i.[1] - i.[0] + 1u)
let ans2 = ~~~ban + 1u
ans2 |> printfn "%d"
