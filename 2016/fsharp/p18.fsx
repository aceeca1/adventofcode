open System.Linq
let c2no(c) = if c = '^' then 1 else 0
let no2c(no) = if no <> 0 then '^' else '.'
let cellA(a: string, n) =
    Array.init(a.Length)(fun i ->
        let pH = if i = 0 then 0 else c2no(a.[i - 1])
        let pM = c2no(a.[i])
        let pL = if i = a.Length - 1 then 0 else c2no(a.[i + 1])
        let p = (pH <<< 2) + (pM <<< 1) + pL
        (n >>> p) &&& 1 |> no2c) |> System.String
let file = new System.IO.StreamReader("../data/input18.txt")
let read = file.ReadLine
let s = read()
let solve(n) =
    let mutable s1 = s
    let mutable ans = 0
    for i = 1 to n do
        ans <- ans + s1.Count(fun i -> i = '.')
        s1 <- cellA(s1, 90)
    ans |> printfn "%d"
solve(40)
solve(400000)
