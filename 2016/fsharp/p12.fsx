let file = new System.IO.StreamReader("../data/input12.txt")
let read = file.ReadLine
let e = Array.init(23)(fun i -> read())
let solve(regC) =
    let reg = System.Collections.Generic.Dictionary<string, int>()
    for i in [|"a"; "b"; "c"; "d"|] do reg.Add(i, 0) |> ignore
    reg.["c"] <- regC
    let mutable c = 0
    let parse(s) =
        match System.Int32.TryParse(s) with
        | true, n -> n
        | _ -> reg.[s]
    while c < e.Length do
        let ec = e.[c].Split()
        match ec.[0] with
        | "cpy" ->
            reg.[ec.[2]] <- parse(ec.[1])
            c <- c + 1
        | "inc" ->
            reg.[ec.[1]] <- reg.[ec.[1]] + 1
            c <- c + 1
        | "dec" ->
            reg.[ec.[1]] <- reg.[ec.[1]] - 1
            c <- c + 1
        | "jnz" ->
            c <- if parse(ec.[1]) = 0 then c + 1 else c + int ec.[2]
        | _ -> failwith "bad input"
    reg.["a"] |> printfn "%d"
solve(0)
solve(1)
