let isTriangle(s1, s2, s3) =
    s1 + s2 > s3 && s2 + s3 > s1 && s3 + s1 > s2
let file = new System.IO.StreamReader("../data/input03.txt")
let read = file.ReadLine
let mutable ans1 = 0
let s = Array.zeroCreate<int[]>(1914)
for i = 0 to s.Length - 1 do
    let nu = null: char[]
    let opt = System.StringSplitOptions.RemoveEmptyEntries
    let si = read().Split(nu, opt) |> Array.map(int)
    if isTriangle(si.[0], si.[1], si.[2]) then ans1 <- ans1 + 1
    s.[i] <- si
ans1 |> printfn "%d"
let mutable ans2 = 0
for i = 0 to s.Length / 3 - 1 do
    let i3 = i * 3
    for j = 0 to 2 do
        if isTriangle(s.[i3].[j], s.[i3 + 1].[j], s.[i3 + 2].[j]) then
            ans2 <- ans2 + 1
ans2 |> printfn "%d"
