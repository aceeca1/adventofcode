let md5er = System.Security.Cryptography.MD5.Create()
let md5(s: string) =
    let a = s |> System.Text.Encoding.ASCII.GetBytes |> md5er.ComputeHash
    let ret = Array.zeroCreate<byte>(32)
    for i = 0 to 15 do
        ret.[i + i] <- a.[i] >>> 4
        ret.[i + i + 1] <- a.[i] &&& 0xfuy
    ret
let cont3(b: byte[]) =
    let rec f(n) =
        if n + 2 >= b.Length then 255uy
        elif b.[n] = b.[n + 1] && b.[n] = b.[n + 2] then b.[n]
        else f(n + 1)
    f(0)
let cont5(b: byte[], c) =
    let rec f(n, v) =
        n < b.Length &&
        let vN = if c = b.[n] then v + 1 else 0
        vN = 5 || f(n + 1, vN)
    f(0, 0)
type HavArr() =
    let mutable i = 0
    let mutable lastSeen = -1
    member u.Add(b) =
        if b then lastSeen <- i
        i <- i + 1
    member u.Have(n) = lastSeen <> -1 && i - lastSeen <= n
let file = new System.IO.StreamReader("../data/input14.txt")
let read = file.ReadLine
let s = read()
let n = 1000
let solve(md5) =
    let a = System.Collections.Generic.Queue<byte[]>()
    let b = Array.init(16)(fun i -> HavArr())
    for i = 0 to n - 1 do
        let mi = md5(s + string i)
        a.Enqueue(mi)
        for j = 0 to 15 do b.[j].Add(cont5(mi, j |> byte))
    let rec f(m, k) =
        let mm = a.Dequeue()
        let mi = md5(s + string(m + n))
        a.Enqueue(mi)
        for j = 0 to 15 do b.[j].Add(cont5(mi, j |> byte))
        let mm3 = cont3(mm)
        if mm3 <> 255uy && b.[int mm3].Have(n) then
            if k = 1 then m else f(m + 1, k - 1)
        else f(m + 1, k)
    f(0, 64) |> printfn "%d"
solve(md5)
let md5s(s: string) =
    md5(s) |> Array.map(fun i ->
        if i < 10uy then byte '0' + i else byte 'a' + (i - 10uy)
        |> char)
    |> System.String
let rec md5sN(s: string, n) =
    if n = 0 then s else md5sN(md5s(s), n - 1)
let md5s2017B(s: string) = md5(md5sN(s, 2016))
solve(md5s2017B)
