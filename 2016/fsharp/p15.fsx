let file = new System.IO.StreamReader("../data/input15.txt")
let read = file.ReadLine
let a = Array.init(6)(fun i ->
    let s = read().Split()
    int s.[3], int s.[11].[.. s.[11].Length - 2])
let solve(a: (int * int)[]) =
    let success(m) =
        (seq {
            for i = 0 to a.Length - 1 do
                let aim, aip = a.[i]
                yield (m + (i + 1) + aip) % aim = 0
        }) |> Seq.forall(id)
    let rec f(m) = if success(m) then m else f(m + 1)
    f(0) |> printfn "%d"
solve(a)
solve([| yield! a; yield (11, 0) |])
