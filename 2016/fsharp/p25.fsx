(* Rewrite the given program using C++ and ... *)
let n = 2534
let mutable x = 2
while x < n do x <- (x <<< 2) + 2
x - n |> printfn "%d"
