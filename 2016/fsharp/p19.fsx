let file = new System.IO.StreamReader("../data/input19.txt")
let read = file.ReadLine
let n = read() |> int
let rec f(m, k) =
    if m = n then k else
        let mN = m + 1
        f(mN, (k + 2) % mN)
f(1, 0) + 1 |> printfn "%d"
let rec g(m, k) =
    if m = n then k else
        let mN = m + 1
        let die = mN >>> 1
        let kN =
            if k < die - 1 then k + 1
            elif k < mN - 2 then k + 2 else 0
        g(mN, kN)
g(1, 0) + 1 |> printfn "%d"
