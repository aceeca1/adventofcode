let file = new System.IO.StreamReader("../data/input10.txt")
let read = file.ReadLine
let a = ResizeArray<Lazy<int>[]>()
let out = ResizeArray<Lazy<int>>()
let addA(k, s) =
    while k >= a.Count do a.Add(null)
    if isNull a.[k] then a.[k] <- Array.zeroCreate<Lazy<int>>(2)
    if isNull a.[k].[0] then a.[k].[0] <- s
    else a.[k].[1] <- s
let addO(k, s) =
    while k >= out.Count do out.Add(null)
    out.[k] <- s
for i = 1 to 231 do
    let s = read().Split()
    match s.[0] with
    | "value" -> addA(int s.[5], lazy(int s.[1]))
    | "bot" ->
        let k = int s.[1]
        let s1 = lazy(min(a.[k].[0].Force())(a.[k].[1].Force()))
        match s.[5] with
        | "bot" -> addA(int s.[6], s1)
        | "output" -> addO(int s.[6], s1)
        | _ -> failwith "bad input"
        let s2 = lazy(max(a.[k].[0].Force())(a.[k].[1].Force()))
        match s.[10] with
        | "bot" -> addA(int s.[11], s2)
        | "output" -> addO(int s.[11], s2)
        | _ -> failwith "bad input"
    | _ -> failwith "bad input"
let mutable ans1 = 0
for i = 0 to a.Count - 1 do
    let v0 = a.[i].[0].Force()
    let v1 = a.[i].[1].Force()
    if v0 = 61 && v1 = 17 || v0 = 17 && v1 = 61 then ans1 <- i
ans1 |> printfn "%d"
let ans2 = out.[0].Force() * out.[1].Force() * out.[2].Force()
ans2 |> printfn "%d"
