#include <cstdio>
using namespace std;

int main() {
    int a = 1, b = 107900, c = 124900, h = 0;
    for (; b <= c; b += 17) {
        int f = 1, d = 2;
        while (true) {
            int e = 2;
            while (true) {
                if (d * e == b) f = 0;
                ++e;
                if (e == b) break;
            }
            ++d;
            if (d == b) break;
        }
        if (!f) ++h;
    }
    printf("%d\n", h);
    return 0;
}
