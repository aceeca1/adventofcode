#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    for (int k = 107900; k <= 124900; k += 17) {
        bool isPrime = true;
        for (int i = 2; i != k; ++i)
            for (int j = 2; j != k; ++j)
                if (i * j == k) isPrime = false;
        if (!isPrime) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
