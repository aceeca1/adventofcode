using System;
using System.IO;
using System.Linq;

class P04 {
    static bool NoDup(string[] s) {
        return s.Distinct().Count() == s.Length;
    }

    static void MapSort(string[] s) {
        for (int i = 0; i < s.Length; ++i) {
            var si = s[i].ToCharArray();
            Array.Sort(si);
            s[i] = new string(si);
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input04.txt");
        int answer1 = 0, answer2 = 0;
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            var s = line.Split();
            if (NoDup(s)) ++answer1;
            MapSort(s);
            if (NoDup(s)) ++answer2;
        }
        Console.WriteLine(answer1);
        Console.WriteLine(answer2);
    }
}
