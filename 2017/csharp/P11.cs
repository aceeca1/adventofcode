using System;
using System.IO;
using System.Linq;

class P11 {
    static int Distance(int[] a) {
        var b = a.Clone() as int[];
        Array.Sort(b);
        var mid = b[1];
        return a.Sum(k => Math.Abs(k - mid));
    }

    public static void Main() {
        var file = new StreamReader("../data/input11.txt");
        var s = file.ReadLine().Split(',');
        var a = new int[3];
        var answer = 0;
        foreach (var i in s) {
            switch (i) {
                case "n":   ++a[0]; break;
                case "nw":  --a[1]; break;
                case "sw":  ++a[2]; break;
                case "s":   --a[0]; break;
                case "se":  ++a[1]; break;
                case "ne":  --a[2]; break;
            }
            var d = Distance(a);
            if (answer < d) answer = d;
        }
        Console.WriteLine(Distance(a));
        Console.WriteLine(answer);
    }
}
