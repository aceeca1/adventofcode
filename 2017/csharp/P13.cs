using System;
using System.Collections.Generic;
using System.IO;

class P13 {
    class Scanner {
        public int Depth, Range;
    }

    static bool Success(List<Scanner> scanner, int n) {
        foreach (var i in scanner)
            if ((i.Depth + n) % ((i.Range - 1) << 1) == 0)
                return false;
        return true;
    }

    public static void Main() {
        var file = new StreamReader("../data/input13.txt");
        var scanner = new List<Scanner>();
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            var noOption = StringSplitOptions.None;
            var lineS = line.Split(new string[] {": "}, noOption);
            scanner.Add(new Scanner {
                Depth = int.Parse(lineS[0]),
                Range = int.Parse(lineS[1])
            });
        }
        int answer1 = 0;
        foreach (var i in scanner)
            if (i.Depth % ((i.Range - 1) << 1) == 0)
                answer1 += i.Depth * i.Range;
        Console.WriteLine(answer1);
        int answer2 = 0;
        while (!Success(scanner, answer2)) ++answer2;
        Console.WriteLine(answer2);
    }
}
