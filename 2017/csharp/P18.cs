using System;
using System.Collections.Generic;
using System.IO;

class P18 {
    class Memory {
        public long[] Reg = new long[26];
        public int Ir = 0;

        public Memory(int id) {
            Reg['p' - 'a'] = id;
        }
    }

    interface Port {
        void Send(long n);
        bool Recover();
        bool Receive(out long n);
    }

    class PortA: Port {
        public long Last;

        public void Send(long n) => Last = n;
        public bool Recover() => true;

        public bool Receive(out long n) { throw new NotImplementedException(); }
    }

    class PortB: Port {
        public Queue<long> In, Out;
        public int Sent = 0;
        public bool NeedData = false;

        public void Send(long n) {
            Out.Enqueue(n);
            ++Sent;
        }

        public bool Recover() => false;

        public bool Receive(out long n) {
            if (In.Count == 0) {
                NeedData = true;
                n = 0;
                return false;
            } else {
                NeedData = false;
                n = In.Dequeue();
                return true;
            }
        }
    }

    class Program: List<Action<Memory, Port>> {
        static int ParseLValue(string s) => s[0] - 'a';

        static Func<Memory, long> ParseRValue(string s) {
            if (char.IsLetter(s[0])) {
                var p = ParseLValue(s);
                return m => m.Reg[p];
            } else {
                var n = long.Parse(s);
                return m => n;
            }
        }

        public static Program Parse(StreamReader file) {
            var answer = new Program();
            while (true) {
                var line = file.ReadLine();
                if (line == null) return answer;
                var lineS = line.Split();
                switch (lineS[0]) {
                    case "snd":
                        var sndX = ParseRValue(lineS[1]);
                        answer.Add((m, p) => {
                            p.Send(sndX(m));
                            ++m.Ir;
                        }); break;
                    case "set":
                        var setX = ParseLValue(lineS[1]);
                        var setY = ParseRValue(lineS[2]);
                        answer.Add((m, p) => {
                            m.Reg[setX] = setY(m);
                            ++m.Ir;
                        }); break;
                    case "add":
                        var addX = ParseLValue(lineS[1]);
                        var addY = ParseRValue(lineS[2]);
                        answer.Add((m, p) => {
                            m.Reg[addX] += addY(m);
                            ++m.Ir;
                        }); break;
                    case "mul":
                        var mulX = ParseLValue(lineS[1]);
                        var mulY = ParseRValue(lineS[2]);
                        answer.Add((m, p) => {
                            m.Reg[mulX] *= mulY(m);
                            ++m.Ir;
                        }); break;
                    case "mod":
                        var modX = ParseLValue(lineS[1]);
                        var modY = ParseRValue(lineS[2]);
                        answer.Add((m, p) => {
                            m.Reg[modX] %= modY(m);
                            ++m.Ir;
                        }); break;
                    case "rcv":
                        var rcvX = ParseLValue(lineS[1]);
                        answer.Add((m, p) => {
                            if (p.Recover()) {
                                if (m.Reg[rcvX] != 0) m.Ir = -1;
                                else ++m.Ir;
                            } else if (p.Receive(out m.Reg[rcvX])) ++m.Ir;
                        }); break;
                    case "jgz":
                        var jgzX = ParseRValue(lineS[1]);
                        var jgzY = ParseRValue(lineS[2]);
                        answer.Add((m, p) => {
                            if (jgzX(m) > 0) m.Ir += (int)jgzY(m);
                            else ++m.Ir;
                        }); break;
                }
            }
        }

        public bool Halt(Memory memory) {
            return !(0 <= memory.Ir && memory.Ir < Count);
        }

        public void RunWith(Memory memory, Port port) {
            while (!Halt(memory)) this[memory.Ir](memory, port);
        }
    }

    class Computer {
        public Program Program;
        public Memory Memory;
        public PortB Port;
    }

    static void Simulate(Computer[] c) {
        while (true) {
            var deadLock = true;
            foreach (var i in c) {
                if (i.Program.Halt(i.Memory)) continue;
                i.Program[i.Memory.Ir](i.Memory, i.Port);
                if (!i.Port.NeedData) deadLock = false;
            }
            if (deadLock) break;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input18.txt");
        var program = Program.Parse(file);
        var portA = new PortA();
        program.RunWith(new Memory(0), portA);
        Console.WriteLine(portA.Last);
        var queue0 = new Queue<long>();
        var queue1 = new Queue<long>();
        var portB0 = new PortB { In = queue0, Out = queue1 };
        var portB1 = new PortB { In = queue1, Out = queue0 };
        Simulate(new Computer[] {
            new Computer {
                Program = program,
                Memory = new Memory(0),
                Port = portB0
            },
            new Computer {
                Program = program,
                Memory = new Memory(1),
                Port = portB1
            }
        });
        Console.WriteLine(portB1.Sent);
    }
}
