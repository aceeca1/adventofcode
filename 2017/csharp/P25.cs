using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P25 {
    class IDGenerator {
        Dictionary<string, int> D = new Dictionary<string, int>();

        public int GetIDFor(string s) {
            if (D.TryGetValue(s, out int v)) return v;
            return D[s] = D.Count;
        }
    }

    class NextEntry {
        public int Write, Move, State;
    }

    class Tape {
        int[] U = {0};
        int Offset = 0;
        public int Size = 1;

        public int Get(int k) => U[Offset + k];
        public void Set(int k, int v) => U[Offset + k] = v;

        void Realloc() {
            var uNew = new int[Size * 3];
            for (int i = 0; i < Size; ++i) uNew[Size + i] = U[Offset + i];
            U = uNew;
            Offset = Size;
        }

        public void AddL() {
            if (Offset == 0) Realloc();
            --Offset;
            ++Size;
        }

        public void AddR() {
            if (Offset + Size == U.Length) Realloc();
            ++Size;
        }

        public int CheckSum() => U.Sum();
    }

    class Turing {
        List<NextEntry[]> Next = new List<NextEntry[]>();
        int State, Cursor = 0, Counter;
        public Tape Tape = new Tape();

        static string LastWord(string s) {
            int space = s.Length - 1;
            while (0 <= space && s[space] != ' ') --space;
            return s.Substring(space + 1, s.Length - 2 - space);
        }

        public Turing(StreamReader file) {
            Func<string> readLastWord = () => LastWord(file.ReadLine());
            var idGen = new IDGenerator();
            State = idGen.GetIDFor(readLastWord());
            Counter = int.Parse(file.ReadLine().Split()[5]);
            while (file.ReadLine() != null) {
                var state = idGen.GetIDFor(readLastWord());
                while (Next.Count <= state) Next.Add(new NextEntry[2]);
                for (int i = 0; i < 2; ++i) {
                    file.ReadLine();
                    Next[state][i] = new NextEntry {
                        Write = int.Parse(readLastWord()),
                        Move = readLastWord() == "left" ? -1 : 1,
                        State = idGen.GetIDFor(readLastWord())
                    };
                }
            }
        }

        public int Run() {
            for (; Counter != 0; --Counter) {
                var next = Next[State][Tape.Get(Cursor)];
                Tape.Set(Cursor, next.Write);
                Cursor += next.Move;
                if (Cursor == -1) {
                    Tape.AddL();
                    Cursor = 0;
                } else if (Cursor == Tape.Size) Tape.AddR();
                State = next.State;
            }
            return Tape.CheckSum();
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input25.txt");
        Console.WriteLine(new Turing(file).Run());
    }
}
