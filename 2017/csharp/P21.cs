using System;
using System.Collections.Generic;
using System.IO;

class P21 {
    class Rule {
        public char[][,] From2x2 = new char[16][,];
        public char[][,] From3x3 = new char[512][,];

        static char[,] Parse(string s) {
            var ss = s.Split('/');
            var answer = new char[ss.Length, ss[0].Length];
            for (int i = 0; i < ss.Length; ++i)
                for (int j = 0; j < ss[0].Length; ++j)
                    answer[i, j] = ss[i][j];
            return answer;
        }

        static char[,] Rotate90(char[,] c) {
            var answer = new char[c.GetLength(1), c.GetLength(0)];
            for (int i = 0; i < c.GetLength(0); ++i)
                for (int j = 0; j < c.GetLength(1); ++j)
                    answer[j, c.GetLength(0) - 1 - i] = c[i, j];
            return answer;
        }

        static char[,] Transpose(char[,] c) {
            var answer = new char[c.GetLength(1), c.GetLength(0)];
            for (int i = 0; i < c.GetLength(0); ++i)
                for (int j = 0; j < c.GetLength(1); ++j)
                    answer[j, i] = c[i, j];
            return answer;
        }

        static IEnumerable<char[,]> Transform(char[,] c) {
            yield return c = Rotate90(c);
            yield return c = Rotate90(c);
            yield return c = Rotate90(c);
            yield return c = Rotate90(c);
            yield return c = Transpose(c);
            yield return c = Rotate90(c);
            yield return c = Rotate90(c);
            yield return c = Rotate90(c);
        }

        static int Encode(char[,] c) {
            int answer = 0;
            for (int i = 0; i < c.GetLength(0); ++i)
                for (int j = 0; j < c.GetLength(1); ++j)
                    answer = answer + answer + (c[i, j] == '#' ? 1 : 0);
            return answer;
        }

        public Rule(StreamReader file) {
            while (true) {
                var line = file.ReadLine();
                if (line == null) break;
                var lineS = line.Split();
                var line0 = Parse(lineS[0]);
                var line2 = Parse(lineS[2]);
                var from = line0.GetLength(0) == 2 ? From2x2 : From3x3;
                foreach (var i in Transform(line0)) from[Encode(i)] = line2;
            }
        }

        public char[,] Enhance(char[,] c) {
            var blockSize = (c.GetLength(0) & 1) == 0 ? 2 : 3;
            var blockSizeNew = blockSize == 2 ? 3 : 4;
            var from = blockSize == 2 ? From2x2 : From3x3;
            var blockCount = c.GetLength(0) / blockSize;
            var sizeNew = blockCount * blockSizeNew;
            var answer = new char[sizeNew, sizeNew];
            for (int i = 0; i < blockCount; ++i)
                for (int j = 0; j < blockCount; ++j) {
                    var block = new char[blockSize, blockSize];
                    for (int i1 = 0; i1 < blockSize; ++i1)
                        for (int j1 = 0; j1 < blockSize; ++j1) {
                            var i2 = i * blockSize + i1;
                            var j2 = j * blockSize + j1;
                            block[i1, j1] = c[i2, j2];
                        }
                    var b = from[Encode(block)];
                    for (int i1 = 0; i1 < blockSizeNew; ++i1)
                        for (int j1 = 0; j1 < blockSizeNew; ++j1) {
                            var i2 = i * blockSizeNew + i1;
                            var j2 = j * blockSizeNew + j1;
                            answer[i2, j2] = b[i1, j1];
                        }
                }
            return answer;
        }
    }

    static char[,] Seed = {{'.', '#', '.'}, {'.', '.', '#'}, {'#', '#', '#'}};

    static int Count(char[,] c) {
        int answer = 0;
        for (int i = 0; i < c.GetLength(0); ++i)
            for (int j = 0; j < c.GetLength(1); ++j)
                answer = answer + (c[i, j] == '#' ? 1 : 0);
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input21.txt");
        var rule = new Rule(file);
        for (int i = 0; i < 5; ++i) Seed = rule.Enhance(Seed);
        Console.WriteLine(Count(Seed));
        for (int i = 5; i < 18; ++i) Seed = rule.Enhance(Seed);
        Console.WriteLine(Count(Seed));
    }
}
