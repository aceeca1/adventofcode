using System;
using System.Collections.Generic;
using System.IO;

class P05 {
    static int Solve1(int[] a) {
        int current = 0, answer = 0;
        while (0 <= current && current < a.Length) {
            ++answer;
            current += a[current]++;
        }
        return answer;
    }

    static int Solve2(int[] a) {
        int current = 0, answer = 0;
        while (0 <= current && current < a.Length) {
            ++answer;
            int next = current + a[current];
            if (a[current] >= 3) --a[current]; else ++a[current];
            current = next;
        }
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input05.txt");
        var a = new List<int>();
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            a.Add(int.Parse(line));
        }
        Console.WriteLine(Solve1(a.ToArray()));
        Console.WriteLine(Solve2(a.ToArray()));
    }
}
