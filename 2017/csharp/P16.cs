using System;
using System.Collections.Generic;
using System.IO;

class P16 {
    class Dance {
        char[] Dancer = new char[16];
        int[] Position = new int[16];
        int ViewFrom = 0;

        public Dance() {
            for (int i = 0; i < 16; ++i) {
                Dancer[i] = (char)('a' + i);
                Position[i] = i;
            }
        }

        public void Spin(int n) {
            ViewFrom = (ViewFrom - n) & 15;
        }

        public void Exchange(int p1, int p2) {
            Swap((p1 + ViewFrom) & 15, (p2 + ViewFrom) & 15);
        }

        public void Partner(char c1, char c2) {
            Swap(Position[c1 - 'a'], Position[c2 - 'a']);
        }

        void Swap(int p1, int p2) {
            char c1 = Dancer[p1];
            char c2 = Dancer[p2];
            Dancer[p1] = c2;
            Dancer[p2] = c1;
            Position[c1 - 'a'] = p2;
            Position[c2 - 'a'] = p1;
        }

        public IEnumerable<char> ToCharSeq() {
            for (int i = ViewFrom; i < 16; ++i) yield return Dancer[i];
            for (int i = 0; i < ViewFrom;  ++i) yield return Dancer[i];
        }
    }

    static Action<Dance> ParseAction(string s) {
        switch (s[0]) {
            case 's':
                var n = int.Parse(s.Substring(1));
                return dance => dance.Spin(n);
            case 'p':
                var c1 = s[1];
                var c2 = s[3];
                return dance => dance.Partner(c1, c2);
            case 'x':
                var p = s.Substring(1).Split('/');
                var p1 = int.Parse(p[0]);
                var p2 = int.Parse(p[1]);
                return dance => dance.Exchange(p1, p2);
        }
        return null;
    }

    static string Process(Action<Dance>[] a, int n) {
        var dance = new Dance();
        var visited = new Dictionary<string, int>();
        for (int i = 0, i0;; ++i) {
            var s = string.Concat(dance.ToCharSeq());
            if (n <= i) return s;
            if (visited != null) {
                if (visited.TryGetValue(s, out i0)) {
                    n = (n - i0) % (i - i0) + i;
                    visited = null;
                } else visited[s] = i;
            }
            foreach (var f in a) f(dance);
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input16.txt");
        var a = Array.ConvertAll(file.ReadLine().Split(','), ParseAction);
        Console.WriteLine(Process(a, 1));
        Console.WriteLine(Process(a, 1000000000));
    }
}
