using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P12 {
    class Graph {
        public List<int[]> E = new List<int[]>();
        bool[] Visited;

        void Visit(int no) {
            Visited[no] = true;
            foreach (var no1 in E[no])
                if (!Visited[no1]) Visit(no1);
        }

        public int Size0() {
            Visited = new bool[E.Count];
            Visit(0);
            return Visited.Count(k => k);
        }

        public int TotalGroups() {
            Visited = new bool[E.Count];
            int answer = 0;
            for (int i = 0; i < E.Count; ++i)
                if (!Visited[i]) { Visit(i); ++answer; }
            return answer;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input12.txt");
        var graph = new Graph();
        for (int i = 0; ; ++i) {
            var line = file.ReadLine();
            if (line == null) break;
            var s = line.Split();
            var targets = new int[s.Length - 2];
            for (int j = 2; j < s.Length; ++j) {
                if (s[j].EndsWith(","))
                    s[j] = s[j].Substring(0, s[j].Length - 1);
                targets[j - 2] = int.Parse(s[j]);
            }
            graph.E.Add(targets);
        }
        Console.WriteLine(graph.Size0());
        Console.WriteLine(graph.TotalGroups());
    }
}
