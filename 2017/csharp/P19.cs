using System;
using System.Collections.Generic;
using System.IO;

class P19 {
    class State {
        public int X, Y, Direction;
    }

    class Diagram {
        List<string> Map = new List<string>();
        public int Count = 1;

        public Diagram(StreamReader file) {
            while (true) {
                var line = file.ReadLine();
                if (line == null) break;
                Map.Add(line);
            }
        }

        static int[] DeltaX = {0, -1, 0, 1};
        static int[] DeltaY = {1, 0, -1, 0};

        bool Valid(State state) {
            if (!(0 <= state.X && state.X < Map.Count)) return false;
            if (!(0 <= state.Y && state.Y < Map[0].Length)) return false;
            return Map[state.X][state.Y] != ' ';
        }

        bool GoNext(State state, out State newState) {
            newState = null;
            foreach (var newDirection in new int[] {
                state.Direction,
                (state.Direction + 1) & 3,
                (state.Direction + 3) & 3
            }) {
                newState = new State {
                    X = state.X + DeltaX[newDirection],
                    Y = state.Y + DeltaY[newDirection],
                    Direction = newDirection
                };
                if (Valid(newState)) return true;
            }
            return false;
        }

        public IEnumerable<char> GoThrough() {
            var state = new State {
                X = 0, Y = Map[0].IndexOf('|'), Direction = 3
            };
            State newState;
            while (GoNext(state, out newState)) {
                state = newState;
                char c = Map[newState.X][newState.Y];
                if (char.IsLetter(c)) yield return c;
                ++Count;
            }
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input19.txt");
        var diagram = new Diagram(file);
        Console.WriteLine(string.Concat(diagram.GoThrough()));
        Console.WriteLine(diagram.Count);
    }
}
