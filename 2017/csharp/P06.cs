using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P06 {
    static void Redistribute(int[] a) {
        var max = a.Max();
        var k = Array.FindIndex(a, k1 => k1 == max);
        a[k] = 0;
        k = (k + 1) % a.Length;
        while (max != 0) {
            ++a[k];
            --max;
            k = (k + 1) % a.Length;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input06.txt");
        var a = Array.ConvertAll(file.ReadLine().Split(), int.Parse);
        var h = new HashSet<string>();
        while (true) {
            var v = string.Join(" ", a);
            if (h.Contains(v)) break;
            h.Add(v);
            Redistribute(a);
        }
        int answer1 = h.Count;
        Console.WriteLine(answer1);
        while (true) {
            var v = string.Join(" ", a);
            if (!h.Contains(v)) break;
            h.Remove(v);
            Redistribute(a);
        }
        Console.WriteLine(answer1 - h.Count);
    }
}
