using System;
using System.IO;

class P01 {
    public static void Main() {
        var file = new StreamReader("../data/input01.txt");
        var s = file.ReadLine();
        int answer1 = 0;
        for (int i = 1; i < s.Length; ++i)
            if (s[i] == s[i - 1]) answer1 += s[i] - '0';
        if (s[0] == s[s.Length - 1]) answer1 += s[0] - '0';
        Console.WriteLine(answer1);
        int answer2 = 0, half = s.Length >> 1;
        for (int i = 0; i < s.Length; ++i)
            if (s[i] == s[(i + half) % s.Length]) answer2 += s[i] - '0';
        Console.WriteLine(answer2);
    }
}
