using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P07 {
    class Program {
        public int Weight;
        public string[] Childs;
    }

    class Tree {
        public Dictionary<string, Program> Dict =
            new Dictionary<string, Program>();
        public string Root;
        public int Correction;

        public void Parse(string line) {
            var s = line.Split();
            var name = s[0];
            var weight = int.Parse(s[1].Substring(1, s[1].Length - 2));
            var childs = new string[Math.Max(s.Length - 3, 0)];
            for (int i = 3; i < s.Length; ++i) {
                if (i != s.Length - 1)
                    s[i] = s[i].Substring(0, s[i].Length - 1);
                childs[i - 3] = s[i];
            }
            Dict[name] = new Program {
                Weight = weight,
                Childs = childs
            };
        }

        public void FindRoot() {
            var a = new HashSet<string>(Dict.Keys);
            foreach (var i in Dict.Values)
                foreach (var j in i.Childs) a.Remove(j);
            Root = a.First();
        }

        public int Balance(string node) {
            var freq = new Dictionary<int, int>();
            if (Dict[node].Childs.Length == 0) return Dict[node].Weight;
            var w = Array.ConvertAll(Dict[node].Childs, Balance);
            foreach (var i in w) {
                int v;
                freq.TryGetValue(i, out v);
                freq[i] = v + 1;
            }
            var max = freq.Values.Max();
            var argmax = freq.First(kv => kv.Value == max).Key;
            if (max != Dict[node].Childs.Length) {
                var arg2 = freq.First(kv => kv.Value != max).Key;
                var name = Dict[node].Childs[Array.IndexOf(w, arg2)];
                var oldW = Dict[name].Weight;
                Correction = oldW - arg2 + argmax;
            }
            return Dict[node].Weight + Dict[node].Childs.Length * argmax;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input07.txt");
        var tree = new Tree();
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            tree.Parse(line);
        }
        tree.FindRoot();
        Console.WriteLine(tree.Root);
        tree.Balance(tree.Root);
        Console.WriteLine(tree.Correction);
    }
}
