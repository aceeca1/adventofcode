using System;
using System.Collections.Generic;
using System.IO;

class P23 {
    class Memory {
        public long[] Reg = new long[8];
        public int Ir = 0;
        public int MulCounter = 0;
    }

    class Program: List<Action<Memory>> {
        static int ParseLValue(string s) => s[0] - 'a';

        static Func<Memory, long> ParseRValue(string s) {
            if (char.IsLetter(s[0])) {
                var p = ParseLValue(s);
                return m => m.Reg[p];
            } else {
                var n = long.Parse(s);
                return m => n;
            }
        }

        public static Program Parse(StreamReader file) {
            var answer = new Program();
            while (true) {
                var line = file.ReadLine();
                if (line == null) return answer;
                var lineS = line.Split();
                switch (lineS[0]) {
                    case "set":
                        var setX = ParseLValue(lineS[1]);
                        var setY = ParseRValue(lineS[2]);
                        answer.Add(m => {
                            m.Reg[setX] = setY(m);
                            ++m.Ir;
                        }); break;
                    case "sub":
                        var subX = ParseLValue(lineS[1]);
                        var subY = ParseRValue(lineS[2]);
                        answer.Add(m => {
                            m.Reg[subX] -= subY(m);
                            ++m.Ir;
                        }); break;
                    case "mul":
                        var mulX = ParseLValue(lineS[1]);
                        var mulY = ParseRValue(lineS[2]);
                        answer.Add(m => {
                            ++m.MulCounter;
                            m.Reg[mulX] *= mulY(m);
                            ++m.Ir;
                        }); break;
                    case "jnz":
                        var jnzX = ParseRValue(lineS[1]);
                        var jnzY = ParseRValue(lineS[2]);
                        answer.Add(m => {
                            if (jnzX(m) != 0) m.Ir += (int)jnzY(m);
                            else ++m.Ir;
                        }); break;
                }
            }
        }

        public bool Halt(Memory memory) {
            return !(0 <= memory.Ir && memory.Ir < Count);
        }

        public void RunWith(Memory memory) {
            while (!Halt(memory)) this[memory.Ir](memory);
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input23.txt");
        var program = Program.Parse(file);
        var memory = new Memory();
        program.RunWith(memory);
        Console.WriteLine(memory.MulCounter);

        int answer = 0;
        for (int k = 107900; k <= 124900; k += 17) {
            bool isPrime = true;
            for (int i = 2; i != k; ++i)
                if (k % i == 0) isPrime = false;
            if (!isPrime) ++answer;
        }
        Console.WriteLine(answer);
    }
}
