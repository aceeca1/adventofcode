using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P24 {
    class Edge {
        public int ST;
        public bool Available;
    }

    class Graph {
        List<Edge>[] Out;

        public Graph(StreamReader file) {
            var a = new List<int[]>();
            while (true) {
                var line = file.ReadLine();
                if (line == null) break;
                a.Add(Array.ConvertAll(line.Split('/'), int.Parse));
            }
            var maxLabel = a.Max(k => k.Max());
            Out = new List<Edge>[maxLabel + 1];
            for (int i = 0; i < Out.Length; ++i) Out[i] = new List<Edge>();
            foreach (var i in a) {
                var e = new Edge { ST = i.Sum(), Available = true };
                Out[i[0]].Add(e);
                Out[i[1]].Add(e);
            }
        }

        public int StrongestPath(int v) {
            int answer = 0;
            foreach (var i in Out[v])
                if (i.Available) {
                    i.Available = false;
                    var answer1 = StrongestPath(i.ST - v) + i.ST;
                    if (answer < answer1) answer = answer1;
                    i.Available = true;
                }
            return answer;
        }

        public int LongestPath(int v, out int strength) {
            int answer = 0;
            strength = 0;
            foreach (var i in Out[v])
                if (i.Available) {
                    i.Available = false;
                    var answer1 = LongestPath(i.ST - v, out int strength1) + 1;
                    strength1 += i.ST;
                    if (answer < answer1) {
                        answer = answer1;
                        strength = strength1;
                    } else if (answer == answer1 && strength < strength1)
                        strength = strength1;
                    i.Available = true;
                }
            return answer;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input24.txt");
        var graph = new Graph(file);
        Console.WriteLine(graph.StrongestPath(0));
        graph.LongestPath(0, out int answer);
        Console.WriteLine(answer);
    }
}
