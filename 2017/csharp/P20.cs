using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P20 {
    class Particle {
        public int[] P, V, A;
        public int AbsP, AbsV, AbsA;

        public Particle(string line) {
            var lineS = line.Split();
            var p = lineS[0].Substring(3, lineS[0].Length - 5).Split(',');
            var v = lineS[1].Substring(3, lineS[1].Length - 5).Split(',');
            var a = lineS[2].Substring(3, lineS[2].Length - 4).Split(',');
            P = Array.ConvertAll(p, int.Parse);
            V = Array.ConvertAll(v, int.Parse);
            A = Array.ConvertAll(a, int.Parse);
            AbsP = P.Sum(Math.Abs);
            AbsV = V.Sum(Math.Abs);
            AbsA = A.Sum(Math.Abs);
        }

        public Particle TickSelf() {
            for (int i = 0; i < 3; ++i) V[i] += A[i];
            for (int i = 0; i < 3; ++i) P[i] += V[i];
            return this;
        }
    }

    class ParticleLongTermDistance: IComparer<Particle> {
        public int Compare(Particle p1, Particle p2) {
            if (p1.AbsA != p2.AbsA) return p1.AbsA.CompareTo(p2.AbsA);
            if (p1.AbsV != p2.AbsV) return p1.AbsV.CompareTo(p2.AbsV);
            return p1.AbsP.CompareTo(p2.AbsP);
        }
    }

    static int ArgMinBy<T>(IEnumerable<T> input, IComparer<T> comparer) {
        var min = default(T);
        int argmin = -1, arg = 0;
        foreach (var i in input) {
            if (argmin == -1 || comparer.Compare(i, min) < 0) {
                min = i;
                argmin = arg;
            }
            ++arg;
        }
        return argmin;
    }

    static ulong Hash(int[] a) {
        const ulong kMul = 0x9ddfea08eb382d69;
        ulong n = 0;
        foreach (var i in a) {
            n = (n ^ (ulong)i) * kMul;
            n ^= n >> 44;
        }
        n *= kMul;
        return n ^ (n >> 41);
    }

    public static void Main() {
        var file = new StreamReader("../data/input20.txt");
        var a = new List<Particle>();
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            a.Add(new Particle(line));
        }
        Console.WriteLine(ArgMinBy(a, new ParticleLongTermDistance()));
        for (int k = 0; k < 100; ++k) {
            var dict = new Dictionary<ulong, Particle>();
            foreach (var i in a) {
                var h = Hash(i.P);
                if (dict.TryGetValue(h, out Particle p)) dict[h] = null;
                else dict[h] = i;
            }
            a.Clear();
            foreach (var i in dict)
                if (i.Value != null) a.Add(i.Value.TickSelf());
        }
        Console.WriteLine(a.Count);
    }
}
