using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P03 {
    static int[] GetXY(int n) {
        if (--n == 0) return new int[] {0, 0};
        var n1 = (int)Math.Sqrt(n);
        if ((n1 & 1) == 0) --n1;
        var k = n - n1 * n1;
        var n2 = k / (n1 + 1);
        var n3 = k % (n1 + 1);
        n1 = (n1 + 1) >> 1;
        switch (n2) {
            case 0: return new int[] {n1, 1 - n1 + n3};
            case 1: return new int[] {n1 - 1 - n3, n1};
            case 2: return new int[] {-n1, n1 - 1 - n3};
            case 3: return new int[] {1 - n1 + n3, -n1};
        }
        return null;
    }

    public static void Main() {
        var file = new StreamReader("../data/input03.txt");
        var n = int.Parse(file.ReadLine());
        Console.WriteLine(GetXY(n).Sum(Math.Abs));
        var a = new Dictionary<string, int>();
        a["0,0"] = 1;
        int answer = 0;
        for (int i = 2; ; ++i) {
            var coordinate = GetXY(i);
            answer = 0;
            for (int j1 = -1; j1 <= 1; ++j1)
                for (int j2 = -1; j2 <= 1; ++j2) {
                    int v;
                    coordinate[0] += j1;
                    coordinate[1] += j2;
                    a.TryGetValue(string.Join(",", coordinate), out v);
                    answer += v;
                    coordinate[0] -= j1;
                    coordinate[1] -= j2;
                }
            a[string.Join(",", coordinate)] = answer;
            if (n < answer) break;
        }
        Console.WriteLine(answer);
    }
}
