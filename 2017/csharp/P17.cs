using System;
using System.Collections.Generic;
using System.IO;

class P17 {
    class SpinLock {
        public int Delta;

        List<int> A = new List<int>{0};
        int Position = 0;

        public void Add(int n) {
            Position = (Position + Delta + 1) % A.Count;
            A.Insert(Position, n);
        }

        public int After(int n) {
            var index = A.FindIndex(k => k == n);
            return A[(index + 1) % A.Count];
        }
    }

    class SpinLock0 {
        public int Delta;

        int AHead = 0, ACount = 1, Position = 0;

        public void Add(int n) {
            Position = (Position + Delta + 1) % ACount;
            if (Position == 0) AHead = n;
            ++ACount;
        }

        public int After0() => AHead;
    }

    public static void Main() {
        var file = new StreamReader("../data/input17.txt");
        var delta = int.Parse(file.ReadLine());
        var spin = new SpinLock { Delta = delta };
        for (int i = 1; i <= 2017; ++i) spin.Add(i);
        Console.WriteLine(spin.After(2017));
        var spin0 = new SpinLock0 { Delta = delta };
        for (int i = 1; i <= 50000000; ++i) spin0.Add(i);
        Console.WriteLine(spin0.After0());
    }
}
