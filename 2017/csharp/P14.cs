using System;
using System.IO;
using System.Linq;

class P14 {
    class CircularList {
        public int[] A = new int[256];
        int Current = 0, Skip = 0;

        public CircularList() {
            for (int i = 0; i < 256; ++i) A[i] = i;
        }

        void LShift(int n) {
            int[] B = new int[256];
            for (int i = 0; i < n; ++i) B[i + 256 - n] = A[i];
            for (int i = n; i < A.Length; ++i) B[i - n] = A[i];
            A = B;
        }

        void RShift(int n) { LShift(256 - n); }

        public void Transform(int length) {
            LShift(Current);
            Array.Reverse(A, 0, length);
            RShift(Current);
            Current = (Current + length + Skip) & 255;
            ++Skip;
        }
    }

    static string KnotHash(string s) {
        s = s + "\x11\x1f\x49\x2f\x17";
        var c = new CircularList();
        for (int i = 0; i < 64; ++i)
            foreach (var j in s) c.Transform(j);
        var dense = new int[16];
        for (int i = 0; i < 16; ++i)
            for (int j = 0; j < 16; ++j)
                dense[i] ^= c.A[(i << 4) + j];
        var bin = Array.ConvertAll(dense,
            k => Convert.ToString(k, 2).PadLeft(8, '0'));
        return String.Concat(bin);
    }

    static void RemoveComponent(char[][] a, int x, int y) {
        a[x][y] = '0';
        if (0 < x && a[x - 1][y] == '1') RemoveComponent(a, x - 1, y);
        if (0 < y && a[x][y - 1] == '1') RemoveComponent(a, x, y - 1);
        if (x < a.Length - 1 && a[x + 1][y] == '1')
            RemoveComponent(a, x + 1, y);
        if (y < a[0].Length - 1 && a[x][y + 1] == '1')
            RemoveComponent(a, x, y + 1);
    }

    public static void Main() {
        var file = new StreamReader("../data/input14.txt");
        var key = file.ReadLine();
        int answer1 = 0;
        var h = new char[128][];
        for (int i = 0; i < 128; ++i) {
            h[i] = KnotHash(String.Concat(key, '-', i)).ToCharArray();
            answer1 += h[i].Count(k => k == '1');
        }
        Console.WriteLine(answer1);
        int answer2 = 0;
        for (int i = 0; i < 128; ++i)
            for (int j = 0; j < 128; ++j)
                if (h[i][j] == '1') { RemoveComponent(h, i, j); ++answer2; }
        Console.WriteLine(answer2);
    }
}
