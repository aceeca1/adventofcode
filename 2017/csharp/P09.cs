using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P09 {
    class Parser {
        public string S;
        int Position = 0, Depth = 0;
        public int Answer1 = 0, Answer2 = 0;

        void ParseGarbage() {
            ++Position; // <
            while (true)
                if (S[Position] == '!') Position += 2;
                else if (S[Position] == '>') break;
                else { ++Answer2; ++Position; }
            ++Position; // >
        }

        public void ParseGroup() {
            ++Position; // {
            Answer1 += ++Depth;
            while (true) {
                if (S[Position] == '}') break;
                else if (S[Position] == '{') ParseGroup();
                else ParseGarbage();
                if (S[Position] == ',') ++Position;
            }
            --Depth;
            ++Position; // }
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input09.txt");
        var parser = new Parser{S = file.ReadLine()};
        parser.ParseGroup();
        Console.WriteLine(parser.Answer1);
        Console.WriteLine(parser.Answer2);
    }
}
