using System;
using System.IO;

class P10 {
    class CircularList {
        public int[] A = new int[256];
        int Current = 0, Skip = 0;

        public CircularList() {
            for (int i = 0; i < 256; ++i) A[i] = i;
        }

        void LShift(int n) {
            int[] B = new int[256];
            for (int i = 0; i < n; ++i) B[i + 256 - n] = A[i];
            for (int i = n; i < A.Length; ++i) B[i - n] = A[i];
            A = B;
        }

        void RShift(int n) { LShift(256 - n); }

        public void Transform(int length) {
            LShift(Current);
            Array.Reverse(A, 0, length);
            RShift(Current);
            Current = (Current + length + Skip) & 255;
            ++Skip;
        }
    }

    static string KnotHash(string s) {
        s = s + "\x11\x1f\x49\x2f\x17";
        var c = new CircularList();
        for (int i = 0; i < 64; ++i)
            foreach (var j in s) c.Transform(j);
        var dense = new int[16];
        for (int i = 0; i < 16; ++i)
            for (int j = 0; j < 16; ++j)
                dense[i] ^= c.A[(i << 4) + j];
        var hex = Array.ConvertAll(dense, k => k.ToString("x2"));
        return String.Concat(hex);
    }

    public static void Main() {
        var file = new StreamReader("../data/input10.txt");
        var line = file.ReadLine();
        var a = Array.ConvertAll(line.Split(','), int.Parse);
        var c = new CircularList();
        foreach (var i in a) c.Transform(i);
        Console.WriteLine(c.A[0] * c.A[1]);
        Console.WriteLine(KnotHash(line));
    }
}
