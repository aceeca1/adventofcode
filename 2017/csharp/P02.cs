using System;
using System.IO;
using System.Linq;

class P02 {
    static int DivideResult(int[] a) {
        for (int i = 0; i < a.Length; ++i)
            for (int j = i + 1; j < a.Length; ++j) {
                if (a[i] % a[j] == 0) return a[i] / a[j];
                if (a[j] % a[i] == 0) return a[j] / a[i];
            }
        return 0;
    }

    public static void Main() {
        var file = new StreamReader("../data/input02.txt");
        int answer1 = 0, answer2 = 0;
        while (true) {
            var s = file.ReadLine();
            if (s == null) break;
            var a = Array.ConvertAll(s.Split(), int.Parse);
            answer1 += a.Max() - a.Min();
            answer2 += DivideResult(a);
        }
        Console.WriteLine(answer1);
        Console.WriteLine(answer2);
    }
}
