using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P15 {
    class Generator {
        ulong Seed, Factor;

        public Generator(StreamReader file, ulong factor) {
            Seed = ulong.Parse(file.ReadLine().Split().Last());
            Factor = factor;
        }

        public Generator(Generator g) {
            Seed = g.Seed;
            Factor = g.Factor;
        }

        public IEnumerable<ulong> Generate() {
            while (true) {
                Seed = Seed * Factor % 2147483647;
                yield return Seed;
            }
        }
    }

    static int Solve1(Generator g1, Generator g2) {
        var e1 = g1.Generate().GetEnumerator();
        var e2 = g2.Generate().GetEnumerator();
        int answer = 0;
        for (int i = 0; i < 40000000; ++i) {
            e1.MoveNext();
            e2.MoveNext();
            if ((e1.Current & 0xffff) == (e2.Current & 0xffff)) ++answer;
        }
        return answer;
    }

    static int Solve2(Generator g1, Generator g2) {
        var e1 = g1.Generate().Where(k => (k & 3) == 0).GetEnumerator();
        var e2 = g2.Generate().Where(k => (k & 7) == 0).GetEnumerator();
        int answer = 0;
        for (int i = 0; i < 5000000; ++i) {
            e1.MoveNext();
            e2.MoveNext();
            if ((e1.Current & 0xffff) == (e2.Current & 0xffff)) ++answer;
        }
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input15.txt");
        var g1 = new Generator(file, 16807);
        var g2 = new Generator(file, 48271);
        Console.WriteLine(Solve1(new Generator(g1), new Generator(g2)));
        Console.WriteLine(Solve2(new Generator(g1), new Generator(g2)));
    }
}
