using System;
using System.Collections.Generic;
using System.IO;

class P22 {
    class Board {
        char[,] U;
        int OffsetX = 0, OffsetY = 0;
        public int SizeX, SizeY;

        public char Get(int x, int y) {
            return U[OffsetX + x, OffsetY + y];
        }

        public void Set(int x, int y, char c) {
            U[OffsetX + x, OffsetY + y] = c;
        }

        void Realloc() {
            int sizeX3 = SizeX * 3, sizeY3 = SizeY * 3;
            var uNew = new char[sizeX3, sizeY3];
            for (int i = 0; i < sizeX3; ++i)
                for (int j = 0; j < sizeY3; ++j) uNew[i, j] = '.';
            for (int i = 0; i < SizeX; ++i)
                for (int j = 0; j < SizeY; ++j)
                    uNew[i + SizeX, j + SizeY] = U[i + OffsetX, j + OffsetY];
            U = uNew;
            OffsetX = SizeX;
            OffsetY = SizeY;
        }

        public void AddR() {
            if (OffsetY + SizeY == U.GetLength(1)) Realloc();
            ++SizeY;
        }

        public void AddU() {
            if (OffsetX == 0) Realloc();
            --OffsetX;
            ++SizeX;
        }

        public void AddL() {
            if (OffsetY == 0) Realloc();
            --OffsetY;
            ++SizeY;
        }

        public void AddD() {
            if (OffsetX + SizeX == U.GetLength(0)) Realloc();
            ++SizeX;
        }

        public Board(StreamReader file) {
            var a = new List<string>();
            while (true) {
                var line = file.ReadLine();
                if (line == null) break;
                a.Add(line);
            }
            U = new char[SizeX = a.Count, SizeY = a[0].Length];
            for (int i = 0; i < SizeX; ++i)
                for (int j = 0; j < SizeY; ++j)
                    U[i, j] = a[i][j];
        }

        public Board(Board b) {
            U = b.U;
            OffsetX = b.OffsetX;
            OffsetY = b.OffsetY;
            SizeX = b.SizeX;
            SizeY = b.SizeY;
            Realloc();
        }
    }

    static int Solve1(Board board) {
        int cX = board.SizeX >> 1, cY = board.SizeY >> 1, cDir = 1;
        int answer = 0;
        for (int i = 0; i < 10000; ++i) {
            if (board.Get(cX, cY) == '#') {
                cDir = (cDir + 3) & 3;
                board.Set(cX, cY, '.');
            } else {
                cDir = (cDir + 1) & 3;
                board.Set(cX, cY, '#');
                ++answer;
            }
            switch (cDir) {
                case 0: if (++cY == board.SizeY) board.AddR(); break;
                case 1: if (cX == 0) board.AddU(); else --cX;  break;
                case 2: if (cY == 0) board.AddL(); else --cY;  break;
                case 3: if (++cX == board.SizeX) board.AddD(); break;
            }
        }
        return answer;
    }

    static int Solve2(Board board) {
        int cX = board.SizeX >> 1, cY = board.SizeY >> 1, cDir = 1;
        int answer = 0;
        for (int i = 0; i < 10000000; ++i) {
            switch (board.Get(cX, cY)) {
                case '.':
                    cDir = (cDir + 1) & 3;
                    board.Set(cX, cY, 'W'); break;
                case 'W':
                    ++answer;
                    board.Set(cX, cY, '#'); break;
                case '#':
                    cDir = (cDir + 3) & 3;
                    board.Set(cX, cY, 'F'); break;
                case 'F':
                    cDir = (cDir + 2) & 3;
                    board.Set(cX, cY, '.'); break;
            }
            switch (cDir) {
                case 0: if (++cY == board.SizeY) board.AddR(); break;
                case 1: if (cX == 0) board.AddU(); else --cX;  break;
                case 2: if (cY == 0) board.AddL(); else --cY;  break;
                case 3: if (++cX == board.SizeX) board.AddD(); break;
            }
        }
        return answer;
    }

    public static void Main() {
        var file = new StreamReader("../data/input22.txt");
        var origin = new Board(file);
        Console.WriteLine(Solve1(new Board(origin)));
        Console.WriteLine(Solve2(new Board(origin)));
    }
}
