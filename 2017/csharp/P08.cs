using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class P08 {
    class Instruction {
        public string Register, CondRegister;
        public int Delta;
        public Func<int, bool> Condition;

        public static Instruction Parse(string line) {
            var s = line.Split();
            var answer = new Instruction {
                Register = s[0],
                CondRegister = s[4],
                Delta = int.Parse(s[2])
            };
            if (s[1] == "dec") answer.Delta = -answer.Delta;
            var s6 = int.Parse(s[6]);
            switch (s[5]) {
                case "<" : answer.Condition = k => k <  s6; break;
                case "<=": answer.Condition = k => k <= s6; break;
                case ">" : answer.Condition = k => k >  s6; break;
                case ">=": answer.Condition = k => k >= s6; break;
                case "==": answer.Condition = k => k == s6; break;
                case "!=": answer.Condition = k => k != s6; break;
            }
            return answer;
        }
    }

    class CPU {
        public Dictionary<string, int> Memory = new Dictionary<string, int>();
        public List<Instruction> Inst = new List<Instruction>();

        public int Run() {
            int highest = 0;
            foreach (var i in Inst) {
                int condValue;
                Memory.TryGetValue(i.CondRegister, out condValue);
                if (i.Condition(condValue)) {
                    int regValue;
                    Memory.TryGetValue(i.Register, out regValue);
                    Memory[i.Register] = regValue + i.Delta;
                }
                if (Memory.Count != 0) {
                    var high = Memory.Values.Max();
                    if (highest < high) highest = high;
                }
            }
            return highest;
        }
    }

    public static void Main() {
        var file = new StreamReader("../data/input08.txt");
        var cpu = new CPU();
        while (true) {
            var line = file.ReadLine();
            if (line == null) break;
            cpu.Inst.Add(Instruction.Parse(line));
        }
        int highest = cpu.Run();
        Console.WriteLine(cpu.Memory.Values.Max());
        Console.WriteLine(highest);
    }
}
